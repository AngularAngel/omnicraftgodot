extends Node

class_name DatumRegistry;


@export var _root_folder: String;

var _entries := []
var _entries_dict := {}


func get_entry_by_id(id: int) -> Datum:
	assert(id >= 0);
	return _entries[id];


func get_entry_by_name(datum_name: String) -> Datum:
	return _entries_dict[datum_name];


func get_behavior_script(datum_name: String, dir = null):
	if dir == null:
		dir = str(_root_folder, "/", datum_name, "/")
	
	return load(str(dir, datum_name, ".gd"))


func create_entry(datum_name: String, behavior_script = null, dir = null) -> Datum:
	if behavior_script == null:
		behavior_script = get_behavior_script(datum_name, dir);
	
	var entry : Datum = behavior_script.new();
	
	# Give the node a deterministic name for networking
	entry.name = datum_name
	entry._display_name = datum_name.capitalize()
	entry._id = len(_entries)
	return entry;


func add_entry(entry: Datum):
	add_child(entry)
	_entries.append(entry)
	_entries_dict[entry.name] = entry;


func create_and_add_entry(datum_name: String, behavior_script = null, dir = null):
	add_entry(create_entry(datum_name, behavior_script, dir));
