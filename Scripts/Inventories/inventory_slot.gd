extends Inventory

class_name InventorySlot


var _item : Item : set = set_item;


func set_item(item):
	if _item != null:
		item_removed.emit();
	_item = item;
	if item != null:
		item_added.emit();
	item_changed.emit()
