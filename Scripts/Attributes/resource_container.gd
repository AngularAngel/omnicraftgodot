extends StatContainer


class_name ResourceContainer


signal full
signal empty


enum FillFrequency {NONE, TIME, ROOM_CLEARED};


var _current_resource: PrimaryStat;
var _maximum_resource: Stat;
var _regen_resource: Stat;


@export var _regen_time := FillFrequency.NONE;
@export var _start_maxed = true;
@export var _capped = true;

# Called when the node enters the scene tree for the first time.
func _ready():
	super._ready()
	if _start_maxed:
		_current_resource.set_base_score(_maximum_resource.get_current_score());


func add_stat(stat):
	if stat.get_name().begins_with("Cur"):
		_current_resource = stat;
		_current_resource.changed.connect(_on_current_stat_changed);
	
	if stat.get_name().begins_with("Max"):
		_maximum_resource = stat;
		_maximum_resource.changed.connect(_on_maximum_stat_changed);
	
	if stat.get_name().ends_with("Regen"):
		_regen_resource = stat;
	super.add_stat(stat);


func _on_current_stat_changed():
	var new_score = _current_resource.get_current_score();
	var prev_score = _current_resource.get_previous_score();
	
	if new_score <= 0 && prev_score > 0:
		emit_signal("empty");
	elif _maximum_resource != null:
		var max_score = _maximum_resource.get_current_score();
		if  new_score >= max_score:
			if _capped and new_score > max_score:
				_current_resource.set_current_score(max_score);
			if prev_score < max_score:
				emit_signal("full");


func _on_maximum_stat_changed():
	var percent_change = _maximum_resource.get_current_score() / _maximum_resource.get_previous_score();
	_current_resource.set_base_score(_current_resource._base_score * percent_change);


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
