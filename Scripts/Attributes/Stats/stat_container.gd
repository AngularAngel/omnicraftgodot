extends Attribute

class_name StatContainer

var stats = {};
var subsidiary = null;

func _enter_tree():
	if is_subsidiary():
		super._enter_tree()
	set_meta("isStatContainer", true)


func _ready():
	if not is_subsidiary():
		recalculate_stats();


func is_subsidiary():
	if subsidiary == null:
		subsidiary = get_parent().get_meta("isStatContainer", false)
	
	return subsidiary;


func add_stat(stat: Attribute):
	stats[stat.get_name()] = stat;
	
	if is_subsidiary():
		get_parent().add_stat(stat);


func get_stat(stat_name):
	if stats.has(stat_name):
		return stats[stat_name];
	else:
		return get_parent().get_stat(stat_name);


func recalculate_stats():
	for stat in stats.values():
		stat._recalculate()
