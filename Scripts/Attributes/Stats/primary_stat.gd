extends Stat

class_name PrimaryStat

@export var _base_score := 100.0 : set = set_base_score;


func set_base_score(new_score):
	if new_score != _base_score:
		_base_score = new_score
		_recalculate()


func _recalculate():
	_current_score = _base_score;
