extends Stat


class_name SecondaryStat


@export var _dependencies = {};
@export var _multiplier: float = 1.0;


func _ready():
	var container = get_parent()
	for dependency in _dependencies.keys():
		container.get_stat(dependency).changed.connect(_recalculate)


func _recalculate():
	var sum = 0.0;
	var container = get_parent()
	for dependency in _dependencies.keys():
		sum += container.get_stat(dependency).get_current_score() * _dependencies[dependency];
	set_current_score(sum * _multiplier)
