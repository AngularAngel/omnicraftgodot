extends Control

signal started_new_game;
signal enter_multiplayer_screen;

func _on_new_game_button_pressed():
	emit_signal("started_new_game");

func _on_exit_button_pressed():
	get_tree().get_root().propagate_notification(NOTIFICATION_WM_CLOSE_REQUEST);
	print(get_tree().get_root());


func _on_multiplayer_button_pressed():
	emit_signal("enter_multiplayer_screen");
