extends Node

const GameScene = preload("res://Scenes/Game/Game.tscn")
const UPNPHelper = preload("res://Scenes/Main/UPNPHelper.gd")

var _game : Game
var _upnp_helper : UPNPHelper

func _notification(what):
	if what == NOTIFICATION_WM_CLOSE_REQUEST:
		get_tree().quit() # default behavior

func _on_title_screen_started_new_game():
	hide_menus()
	
	add_child(GameScene.instantiate())

func _on_title_screen_enter_multiplayer_screen():
	hide_menus()
	$MultiplayerScreen.show()

func _on_multiplayer_screen_connect_to_server_requested(ip: String, port: int):
	_game = GameScene.instantiate()
	_game.set_ip(ip)
	_game.set_port(port)
	_game.set_network_mode(Game.NETWORK_MODE_CLIENT)
	add_child(_game)

	hide_menus()

	get_viewport().get_window().title = "Client"

func _on_multiplayer_screen_host_server_requested(port: int):
	if _upnp_helper != null and not _upnp_helper.is_setup():
		_upnp_helper.setup(port, PackedStringArray(["UDP"]), "Omnicraft", 20 * 60)
	
	_game = GameScene.instantiate()
	_game.set_port(port)
	_game.set_network_mode(Game.NETWORK_MODE_HOST)
	add_child(_game)

	hide_menus()

	get_viewport().get_window().title = "Server"

func _on_multiplayer_screen_upnp_toggled(pressed: bool):
	if pressed:
		if _upnp_helper == null:
			_upnp_helper = UPNPHelper.new()
			add_child(_upnp_helper)
	else:
		if _upnp_helper != null:
			_upnp_helper.queue_free()
			_upnp_helper = null

func hide_menus():
	$TitleScreen.hide()
	$MultiplayerScreen.hide()
