extends "res://Scenes/Game/voxels/materials/constants.gd"

const Voxel_Component = preload("./materials/component.gd")
const Voxel_Material = preload("./materials/material.gd")


func generate_stone_material(random: RandomNumberGenerator, stone_name) -> Voxel_Material:
	var material := Voxel_Material.new();
	material.name = stone_name;
	var old_component := Voxel_Component.new();
	old_component.specular = 300.0 / 255.0
	for i in range(5):
		var new_component = Voxel_Component.new();
		var brightening = 0.007 + random.randf() * 0.05;
		new_component.albedo = old_component.albedo + Color(
			brightening + random.randf() * 0.145, 
			brightening + random.randf() * 0.13, 
			brightening + random.randf() * 0.115, 0);
		new_component.specular = old_component.specular - random.randf() * (100.0 / 255.0)
		new_component.roughness = old_component.roughness + random.randf() * (100.0 / 255.0)
		material.add_component(new_component, 0.2);
		old_component = new_component
		
	return material;

func generate_dirt_material(random: RandomNumberGenerator):
	var material = Voxel_Material.new();
	var old_component = Voxel_Component.new();
	old_component.albedo = Color(25.0 / 255.0, 5.0 / 255.0, 0, 1)
	for i in range(5):
		var new_component = Voxel_Component.new();
		var brightening = 0.005 + random.randf() * 0.035;
		new_component.albedo = old_component.albedo + Color(
			brightening + random.randf() * 0.13,
			brightening + random.randf() * 0.09,
			brightening + random.randf() * 0.045, 0);
		material.add_component(new_component, 0.2);
		old_component = new_component
		
	return material;
