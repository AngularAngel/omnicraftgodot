extends Node

const Block_Feature = preload("./features/block_feature.gd")
const Homogenous_Multiblock_Feature = preload("./features/homogenous_multiblock_feature.gd")
const Multiblock_Feature = preload("./features/multiblock_feature.gd")
const Voxel_Fillers = preload("../floating_island/voxel_fillers/voxel_fillers.gd")
const Texture_Definitions = preload("./texture_definitions.gd")
const Voxel_Family = preload("./materials/voxel_family.gd")
const Fluid_Type = preload("./materials/fluid_type.gd")
const Stone_Type = preload("./materials/stone_type.gd")
const Dirt_Type = preload("./materials/dirt_type.gd")
const Grass_Type = preload("./materials/grass_type.gd")
const Plant_Type = preload("./materials/plant_type.gd")
const Glass_Type = preload("./materials/glass_type.gd")
const Meshes = preload("./meshes.gd")
const Shaders = preload("./shaders/shaders.gd")

@onready var _interaction_methods = $Interactions

var _voxel_library : VoxelBlockyLibrary = preload("res://Scenes/Game/VoxelLibrary.tres")
var _side_library : VoxelSideLibrary = preload("res://Scenes/Game/SideLibrary.tres")

var voxel_count := 0;

var _texture_definitions := Texture_Definitions.new();
var _meshes := Meshes.new();
var _shaders := Shaders.new();

var _feature_dict = {}
var _feature_conditions = {}
var _placement_modifiers = {}

var _voxel_fillers: Voxel_Fillers

var _multiblock_x;
var _multiblock_y;
var _multiblock_z;

var _voxel_families := []
var _voxel_family_dict := {}


# Called when the node enters the scene tree for the first time.
func _ready():
	generate_fluid_type(get_parent()._random, "Water")
	
	generate_stone_type(get_parent()._random, "Stone")
	
	generate_dirt_type(get_parent()._random, "Dirt")
	
	generate_grass_type(get_parent()._random, "Grass")
	
	generate_plant_type(get_parent()._random, "Bush")
	
	generate_glass_type(get_parent()._random, "Glass")
	
	_meshes.initialize()
	
	initialize_feature_conditions();
	
	initialize_placement_modifiers();

	var _air = addVoxel("Air")
	
	_multiblock_x = addMultiblockVoxel("Multiblock X")
	_multiblock_y = addMultiblockVoxel("Multiblock Y")
	_multiblock_z = addMultiblockVoxel("Multiblock Z")
	
	for voxel_family in _voxel_families:
		voxel_family.initialize(self);
	
	_voxel_library.atlas_size = 1;
	_voxel_library.bake()
	
	_voxel_fillers = Voxel_Fillers.new();
	
	_voxel_fillers.initialize()
	
func initialize_feature_conditions():
	var within_bounds = func within_bounds(feature, generator, position, _random):
		var dimensions = feature.get_dimensions()
		for x in range(-(dimensions.x - 1), generator._max_horizontal_feature_dimension):
			for z in range(-(dimensions.x - 1), generator._max_horizontal_feature_dimension):
				if not generator.block_within_bounds(position.x + x, position.z + z):
						return false;
		return true;
		
	_feature_conditions["Within Bounds"] = within_bounds
	
	var no_overlapping_features = func no_overlapping_features(feature, generator, position, _random):
		var dimensions = feature.get_dimensions()
		for x in range(-(dimensions.x - 1), generator._max_horizontal_feature_dimension):
			for z in range(-(dimensions.x - 1), generator._max_horizontal_feature_dimension):
				var structures = generator._get_structures_at(position.x - x, position.z - z);
				for structure in structures:
					var other_dimensions = structure.get_dimensions();
					if other_dimensions.x > x && other_dimensions.z > z && \
							(structure.offset.y + other_dimensions.y > position.y and \
							structure.offset.y < position.y + dimensions.y):
						return false;
		return true;
		
	_feature_conditions["No Overlapping Features"] = no_overlapping_features
	
	var no_features_beneath = func no_features_beneath(feature, generator, position, _random):
		var dimensions = feature.get_dimensions()
		for x in range(-(dimensions.x - 1), generator._max_horizontal_feature_dimension):
			for z in range(-(dimensions.x - 1), generator._max_horizontal_feature_dimension):
				var structures = generator._get_structures_at(position.x - x, position.z - z);
				for structure in structures:
					var other_dimensions = structure.get_dimensions();
					if other_dimensions.x > x && other_dimensions.z > z && \
							(structure.offset.y + other_dimensions.y > position.y - 1 and \
							structure.offset.y < position.y + dimensions.y):
						return false;
		return true;
		
	_feature_conditions["No Features Beneath"] = no_features_beneath
	
	var dirt_support = func dirt_support(feature, generator, position, _random):
		var dimensions = feature.get_dimensions()
		for x in range(dimensions.x):
			for z in range(dimensions.z):
				if generator._get_height_at(position.x + x, position.z + z) - 1 != position.y:
					return false;
				var surface = generator._get_surface_at(position.x + x, position.z + z);
				var model = _voxel_library.get_model(surface.voxels.get_voxel(0, 8, 0, feature._Block_Channel));
				if model == null or not model.get_meta("Is_Dirt", false):
					return false;
		return true;
		
	_feature_conditions["Dirt Support"] = dirt_support
	
	var is_on_ground = func is_on_ground(feature, generator, position, _random):
		var dimensions = feature.get_dimensions()
		for x in range(dimensions.x):
			for z in range(dimensions.z):
				if generator._get_height_at(position.x + x, position.z + z) - 1 < position.y:
					return false;
		return true;
		
	_feature_conditions["Is On Ground"] = is_on_ground
		
	var percent_chance = func percent_chance(feature, _generator, _position, random):
		return random.randf() < feature.get_meta("Chance", 0.1)
		
	_feature_conditions["Percent Chance"] = percent_chance

func initialize_placement_modifiers():
	var fully_on_ground = func fully_on_ground(feature, generator, position, _random):
		var dimensions = feature.get_dimensions()
		for x in range(dimensions.x):
			for z in range(dimensions.z):
				position.y = min(position.y, generator._get_height_at(position.x + x, position.z + z) - 1)
		return position;
		
	_placement_modifiers["Fully On Ground"] = fully_on_ground
		
	var fully_in_ground = func fully_in_ground(feature, generator, position, _random):
		var dimensions = feature.get_dimensions()
		for x in range(dimensions.x):
			for z in range(dimensions.z):
				position.y = min(position.y, generator._get_height_at(position.x + x, position.z + z) - (1 + dimensions.y))
		return position;
		
	_placement_modifiers["Fully In Ground"] = fully_in_ground
		
	var floating = func floating(_feature, _generator, position, random):
		position.y += random.randi_range(20, 50);
		return position;
		
	_placement_modifiers["Floating"] = floating
	
func get_voxel_family_dict(family_type: String):
	if not _voxel_family_dict.has(family_type):
		_voxel_family_dict[family_type] = [];
	return _voxel_family_dict[family_type];

func generate_fluid_type(random: RandomNumberGenerator, fluid_name: String):
	var fluid_type = Fluid_Type.new(fluid_name);
	_voxel_families.append(fluid_type)
	get_voxel_family_dict("Fluid Types").append(fluid_type)

func generate_stone_type(random: RandomNumberGenerator, stone_name: String):
	var stone_type = Stone_Type.new(stone_name);
	stone_type.set_meta("voxelMaterial", _texture_definitions.generate_stone_material(random, stone_type._name))
	_voxel_families.append(stone_type)
	get_voxel_family_dict("Stone Types").append(stone_type)


func generate_dirt_type(random: RandomNumberGenerator, dirt_name: String):
	var dirt_type = Dirt_Type.new(dirt_name);
	dirt_type.set_meta("voxelMaterial", _texture_definitions.generate_dirt_material(random))
	_voxel_families.append(dirt_type)
	get_voxel_family_dict("Dirt Types").append(dirt_type)


func generate_grass_type(random: RandomNumberGenerator, grass_name: String):
	var grass_type = Grass_Type.new(grass_name);
	var grass_color = Color(random.randf_range(0, 0.125), 0.10 + random.randf_range(0, 0.45), random.randf_range(0, 0.125), 1)
	grass_type.set_meta("grassColor", grass_color);
	_voxel_families.append(grass_type)
	get_voxel_family_dict("Grass Types").append(grass_type)


func generate_plant_type(random: RandomNumberGenerator, plant_name: String):
	var plant_type = Plant_Type.new(plant_name)
	var plant_color = Color(random.randf_range(0, 0.125), 0.10 + random.randf_range(0, 0.45), random.randf_range(0, 0.125), 1)
	plant_type.set_meta("plantColor", plant_color);
	_voxel_families.append(plant_type)
	get_voxel_family_dict("Plant Types").append(plant_type)

func generate_glass_type(random: RandomNumberGenerator, glass_name):
	var glass_type = Glass_Type.new(glass_name);
	_voxel_families.append(glass_type)
	get_voxel_family_dict("Glass Types").append(glass_type)

func addVoxel(voxelName: String, voxel_family: Voxel_Family = null):
	var voxel = VoxelBlockyModelMesh.new();
	voxel.resource_name = voxelName;
	var id = _voxel_library.add_model(voxel);
	voxel.set_meta("ID", id)
	if voxel_family != null:
		voxel.set_meta("voxelFamily", voxel_family);
	voxel_count += 1;
	return voxel


func addMultiblockVoxel(voxelName : String, voxel_family: Voxel_Family = null, collision_mask := 2) -> VoxelBlockyModel:
	var multiblock_voxel = addVoxel(voxelName, voxel_family)
	multiblock_voxel.set_collision_aabbs([AABB(Vector3(0, 0, 0), Vector3(1, 1, 1)),])
	multiblock_voxel.collision_mask = collision_mask;
	return multiblock_voxel


func addCubeVoxel(voxelName : String, voxel_family: Voxel_Family, collision_mask := 3) -> VoxelBlockyModel:
	var cube_voxel = addVoxel(voxelName, voxel_family)
	cube_voxel.mesh = _meshes._dict["Cube"]
	cube_voxel.set_collision_aabbs([AABB(Vector3(0, 0, 0), Vector3(1, 1, 1)),])
	cube_voxel.collision_mask = collision_mask;
	return cube_voxel
	

func addSide(sideName: String):
	var side = VoxelSideModel.new();
	side.resource_name = sideName;
	var id = _side_library.add_model(side);
	side.set_meta("ID", id)
	return side


var _transparency_index = 1;

func get_new_transparency_index():
	var transparency_index = _transparency_index;
	_transparency_index += 1;
	return transparency_index;

func addTransparentVoxel(voxelName : String, voxel_family: Voxel_Family, collision_mask := 3) -> VoxelBlockyModel:
	var transparent_voxel := addCubeVoxel(voxelName, voxel_family)
	transparent_voxel.transparency_index = get_new_transparency_index();
	transparent_voxel.transparent = true;
	transparent_voxel.set_collision_aabbs([AABB(Vector3(0, 0, 0), Vector3(1, 1, 1)),])
	transparent_voxel.collision_mask = collision_mask;
	return transparent_voxel


func addFluidVoxel(voxelName : String, voxel_family: Voxel_Family, collision_mask := 2) -> VoxelBlockyModel:
	var fluid_voxel := addCubeVoxel(voxelName, voxel_family)
	fluid_voxel.set_collision_aabbs([])
	fluid_voxel.transparency_index = get_new_transparency_index();
	fluid_voxel.transparent = true;
	fluid_voxel.set_collision_aabbs([AABB(Vector3(0, 0, 0), Vector3(1, 1, 1)),])
	fluid_voxel.collision_mask = collision_mask;
	return fluid_voxel


func addPlantVoxel(voxelName : String, voxel_family: Voxel_Family, mesh : Mesh, collision_aabbs = [AABB(Vector3(0, 0, 0), Vector3(1, 1, 1)),], collision_mask := 2) -> VoxelBlockyModel:
	var plant_voxel = addVoxel(voxelName, voxel_family)
	plant_voxel.mesh = mesh
	plant_voxel.set_collision_aabbs(collision_aabbs)
	plant_voxel.set_meta("selectionSize", collision_aabbs[0].size)
	plant_voxel.collision_mask = collision_mask;
	return plant_voxel


func addMiscVoxel(voxelName : String, voxel_family: Voxel_Family, mesh : Mesh, selection_size =  Vector3(1, 1, 1), collision_mask := 3) -> VoxelBlockyModel:
	var misc_voxel = addVoxel(voxelName, voxel_family)
	misc_voxel.mesh = mesh
	misc_voxel.set_meta("selectionSize", selection_size)
	misc_voxel.collision_mask = collision_mask;
	return misc_voxel


func create_block_feature(block_id):
	var block_feature = Block_Feature.new()
	
	block_feature._block_id = block_id
	
	return block_feature;


func create_homogenous_multiblock_feature(block_id, dimensions):
	var homogenous_multiblock_feature = Homogenous_Multiblock_Feature.new()
	
	homogenous_multiblock_feature._block_id = block_id
	homogenous_multiblock_feature._dimensions = dimensions
	
	return homogenous_multiblock_feature;


func create_multiblock_feature(block_id, dimensions):
	var multiblock_feature = Multiblock_Feature.new()
	
	multiblock_feature._block_id = block_id
	multiblock_feature._dimensions = dimensions
	multiblock_feature._multiblock_x = _multiblock_x.get_meta("ID")
	multiblock_feature._multiblock_y = _multiblock_y.get_meta("ID")
	multiblock_feature._multiblock_z = _multiblock_z.get_meta("ID")
	
	return multiblock_feature;
