extends "res://Scenes/Game/voxels/materials/voxel_family.gd"


func initialize(voxels):
	var air = voxels._voxel_library.get_model(0);
	
	var _item_db : ItemDB = voxels.get_parent().find_child("ItemTypes")
	
	var	grass_bundle = _item_db.get_entry_by_name("grass_bundle")
	
	var grass = voxels.addPlantVoxel(_name, self, voxels._meshes._dict["Cross"])
	grass.set_meta("isHarvestable", true);
	grass.set_meta("harvestItem", grass_bundle);
	grass.set_meta("harvestBlock", air);
	_voxel_ids["Grass"] = grass.get_meta("ID")
	
	var large_grass = voxels.addPlantVoxel("Large " + _name, self, voxels._meshes._dict["Large Cross"], [AABB(Vector3(0, 0, 0), Vector3(2, 2, 2)),]);
	large_grass.set_meta("isHarvestable", true);
	large_grass.set_meta("harvestItem", grass_bundle);
	large_grass.set_meta("harvestBlock", air);
	_voxel_ids["Large Grass"] = large_grass.get_meta("ID")
	
	var grass_material = voxels._shaders.get_new_shader_material(_name, voxels._shaders._vegetation_shader)
	
	grass_material.set_shader_parameter("plant_color", get_meta("grassColor"))
	
	grass.set_material_override(0, grass_material);
	large_grass.set_material_override(0, grass_material);
	
	var top_grass_side_material = voxels._shaders.get_new_shader_material(_name, voxels._shaders._top_grass_shader)
	
	top_grass_side_material.set_shader_parameter("plant_color", get_meta("grassColor"))
	
	var top_grass_side = voxels.addSide(_name + " Top")
	_voxel_ids["Top Side"] = top_grass_side.get_meta("ID");
	
	top_grass_side.material = top_grass_side_material;
	
	var downwards_grass_side_material = voxels._shaders.get_new_shader_material(_name, voxels._shaders._side_grass_shader)
	
	downwards_grass_side_material.set_shader_parameter("plant_color", get_meta("grassColor"))
	
	var downwards_grass_side = voxels.addSide(_name + " Downwards")
	_voxel_ids["Downwards Side"] = downwards_grass_side.get_meta("ID");
	
	downwards_grass_side.material = downwards_grass_side_material;
	
	var large_grass_feature = voxels.create_multiblock_feature(_voxel_ids["Large Grass"], Vector3i(2, 2, 2))
	large_grass_feature._conditions.append(voxels._feature_conditions["Within Bounds"])
	large_grass_feature._conditions.append(voxels._feature_conditions["No Features Beneath"])
	large_grass_feature._conditions.append(voxels._feature_conditions["Dirt Support"])
	large_grass_feature._conditions.append(voxels._feature_conditions["Percent Chance"])
	large_grass_feature.set_meta("Chance", 0.015)
	
	voxels._feature_dict["Large " + _name] = large_grass_feature
	_features["Large Grass"] = large_grass_feature
	
	var grass_feature = voxels.create_block_feature(_voxel_ids["Grass"])
	grass_feature._conditions.append(voxels._feature_conditions["No Features Beneath"])
	grass_feature._conditions.append(voxels._feature_conditions["Dirt Support"])
	grass_feature._conditions.append(voxels._feature_conditions["Percent Chance"])
	grass_feature.set_meta("Chance", 0.035)
	
	voxels._feature_dict[_name] = grass_feature
	_features["Grass"] = grass_feature
