extends "res://Scenes/Game/voxels/materials/voxel_family.gd"

func initialize(voxels):
	var glass = voxels.addTransparentVoxel("Glass", self)
	
	var glass_material = voxels._shaders.get_new_shader_material("Glass", voxels._shaders._glass_shader);
		
	glass.set_material_override(0, glass_material);
