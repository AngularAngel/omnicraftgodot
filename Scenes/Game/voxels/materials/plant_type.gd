extends "res://Scenes/Game/voxels/materials/voxel_family.gd"


func initialize(voxels):
	var air = voxels._voxel_library.get_model(0);
	
	var _item_db : ItemDB = voxels.get_parent().find_child("ItemTypes")
	
	var	grass_bundle = _item_db.get_entry_by_name("grass_bundle")
	var	berries = _item_db.get_entry_by_name("berries")
	
	var bush = voxels.addPlantVoxel(_name, self, voxels._meshes._dict["Grid"]);
	bush.set_meta("isHarvestable", true);
	bush.set_meta("harvestItem", grass_bundle);
	bush.set_meta("harvestBlock", air);
	_voxel_ids["Bush"] = bush.get_meta("ID")
	
	var berry_bush = voxels.addPlantVoxel("Berry " + _name, self, voxels._meshes._dict["Berry Bush"]);
	berry_bush.set_meta("isHarvestable", true);
	berry_bush.set_meta("harvestItem", berries);
	berry_bush.set_meta("harvestBlock", bush);
	_voxel_ids["Berry Bush"] = berry_bush.get_meta("ID")
	
	var large_bush = voxels.addPlantVoxel("Large " + _name, self, voxels._meshes._dict["Large Grid"], [AABB(Vector3(0, 0, 0), Vector3(2, 1, 2)),])
	large_bush.set_meta("isHarvestable", true);
	large_bush.set_meta("harvestItem", grass_bundle);
	large_bush.set_meta("harvestBlock", air);
	_voxel_ids["Large Bush"] = large_bush.get_meta("ID")
	
	var plant_material = voxels._shaders.get_new_shader_material(_name + " Bush", voxels._shaders._vegetation_shader)
	
	plant_material.set_shader_parameter("plant_color", get_meta("plantColor"))
	
	bush.set_material_override(0, plant_material)
	berry_bush.set_material_override(0, plant_material)
	large_bush.set_material_override(0, plant_material)
	
	var berry_material = voxels._shaders.get_new_shader_material(_name + " Berry", voxels._shaders._berry_shader)
	
	berry_bush.set_material_override(1, berry_material)
	
	var large_bush_feature = voxels.create_multiblock_feature(_voxel_ids["Large Bush"], Vector3i(2, 1, 2))
	large_bush_feature._conditions.append(voxels._feature_conditions["Within Bounds"])
	large_bush_feature._conditions.append(voxels._feature_conditions["No Features Beneath"])
	large_bush_feature._conditions.append(voxels._feature_conditions["Dirt Support"])
	large_bush_feature._conditions.append(voxels._feature_conditions["Percent Chance"])
	large_bush_feature.set_meta("Chance", 0.01)
	
	voxels._feature_dict["Large " + _name + " Bush"] = large_bush_feature
	_features["Large Bush"] = large_bush_feature
	
	var bush_feature = voxels.create_block_feature(_voxel_ids["Bush"])
	bush_feature._conditions.append(voxels._feature_conditions["No Features Beneath"])
	bush_feature._conditions.append(voxels._feature_conditions["Dirt Support"])
	bush_feature._conditions.append(voxels._feature_conditions["Percent Chance"])
	bush_feature.set_meta("Chance", 0.03)
	
	voxels._feature_dict[_name + " Bush"] = bush_feature
	_features["Bush"] = bush_feature
	
	var berry_bush_feature = voxels.create_block_feature(_voxel_ids["Berry Bush"])
	berry_bush_feature._conditions.append(voxels._feature_conditions["No Features Beneath"])
	berry_bush_feature._conditions.append(voxels._feature_conditions["Dirt Support"])
	berry_bush_feature._conditions.append(voxels._feature_conditions["Percent Chance"])
	berry_bush_feature.set_meta("Chance", 0.03)
	
	voxels._feature_dict[_name + "Berry Bush"] = berry_bush_feature
	_features["Berry Bush"] = berry_bush_feature
