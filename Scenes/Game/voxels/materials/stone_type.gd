extends "res://Scenes/Game/voxels/materials/voxel_family.gd"

func initialize(voxels):
	var stone = voxels.addCubeVoxel(_name, self);
	_voxel_ids["Basic"] = stone.get_meta("ID")
	
	var stone_material = voxels._shaders.get_new_shader_material(_name, voxels._shaders._earth_shader);
	
	get_meta("voxelMaterial").set_shader_material_parameters(stone_material);
	
	stone_material.set_shader_parameter("layers", [
		Vector4(1024, 1024, 1024, 0),
		Vector4(32, 2.5, 32, 0.666),
		Vector4(0, 32, 0, 0),
		Vector4(3, 8, 6, 0.333),
		Vector4(),
		Vector4(0.025, 0, 0, 0)
	])
	
	stone.set_material_override(0, stone_material);
	var small_stone = voxels.addMiscVoxel("Small " + _name, self, voxels._meshes._dict["Small Cube"])
	small_stone.set_collision_aabbs([AABB(
			Vector3(4.0 / 16.0, 0, 5.0 / 16.0),
			Vector3(13.0 / 16.0, 9.0 / 16.0, 13.0 / 16.0)
	),])
	small_stone.set_material_override(0, stone_material)
	_voxel_ids["Small"] = small_stone.get_meta("ID")
	
	var large_rock = voxels.create_homogenous_multiblock_feature(_voxel_ids["Basic"], Vector3i(2, 2, 2))
	large_rock._conditions.append(voxels._feature_conditions["Within Bounds"])
	large_rock._conditions.append(voxels._feature_conditions["No Overlapping Features"])
	large_rock._conditions.append(voxels._feature_conditions["Percent Chance"])
	large_rock.set_meta("Chance", 0.01)
	large_rock._placement_modifiers.append(voxels._placement_modifiers["Fully On Ground"])
	
	voxels._feature_dict["Large " + _name] = large_rock
	_features["Large Rock"] = large_rock
	
	var small_rock = voxels.create_block_feature(_voxel_ids["Basic"])
	small_rock._conditions.append(voxels._feature_conditions["No Overlapping Features"])
	small_rock._conditions.append(voxels._feature_conditions["Percent Chance"])
	small_rock.set_meta("Chance", 0.015)
	small_rock._placement_modifiers.append(voxels._placement_modifiers["Fully On Ground"])
	
	voxels._feature_dict["Small " + _name] = small_rock
	_features["Small Rock"] = small_rock
	
	var smaller_rock = voxels.create_block_feature(_voxel_ids["Small"])
	smaller_rock._conditions.append(voxels._feature_conditions["No Overlapping Features"])
	smaller_rock._conditions.append(voxels._feature_conditions["Percent Chance"])
	smaller_rock.set_meta("Chance", 0.015)
	smaller_rock._placement_modifiers.append(voxels._placement_modifiers["Fully On Ground"])
	
	voxels._feature_dict["Smaller " + _name] = smaller_rock
	_features["Smaller Rock"] = smaller_rock
	
	var giant_floating_rock = large_rock.copy()
	giant_floating_rock._dimensions = Vector3i(4, 4, 4)
	giant_floating_rock.set_meta("Chance", 0.01)
	giant_floating_rock._placement_modifiers.clear()
	giant_floating_rock._placement_modifiers.append(voxels._placement_modifiers["Floating"])
	
	voxels._feature_dict["Giant Floating " + _name] = giant_floating_rock
	_features["Giant Floating Rock"] = giant_floating_rock
