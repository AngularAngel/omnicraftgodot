extends "res://Scenes/Game/voxels/materials/voxel_family.gd"

func initialize(voxels):
	var dirt = voxels.addCubeVoxel(_name, self)
	dirt.set_meta("Is_Dirt", true);
	_voxel_ids["Basic"] = dirt.get_meta("ID")
	
	var dirt_material = voxels._shaders.get_new_shader_material(_name, voxels._shaders._earth_shader);
	
	get_meta("voxelMaterial").set_shader_material_parameters(dirt_material);
	
	dirt_material.set_shader_parameter("layers", [
		Vector4(1024, 1024, 1024, 0),
		Vector4(32, 2.5, 32, 0.666),
		Vector4(0, 32, 0, 0),
		Vector4(3, 8, 6, 0.333),
		Vector4(),
		Vector4(0.025, 0, 0, 0)
	])
	
	dirt.set_material_override(0, dirt_material);
	
	var large_dirt_patch = voxels.create_homogenous_multiblock_feature(_voxel_ids["Basic"], Vector3i(2, 1, 2))
	large_dirt_patch._conditions.append(voxels._feature_conditions["Within Bounds"])
	large_dirt_patch._conditions.append(voxels._feature_conditions["No Overlapping Features"])
	large_dirt_patch._conditions.append(voxels._feature_conditions["Percent Chance"])
	large_dirt_patch.set_meta("Chance", 0.01)
	large_dirt_patch._placement_modifiers.append(voxels._placement_modifiers["Fully In Ground"])
	
	voxels._feature_dict["Large " + _name + " Patch"] = large_dirt_patch
	_features["Large Dirt Patch"] = large_dirt_patch
	
	var small_dirt_patch = voxels.create_block_feature(_voxel_ids["Basic"])
	small_dirt_patch._conditions.append(voxels._feature_conditions["No Overlapping Features"])
	small_dirt_patch._conditions.append(voxels._feature_conditions["Percent Chance"])
	small_dirt_patch.set_meta("Chance", 0.015)
	small_dirt_patch._placement_modifiers.append(voxels._placement_modifiers["Fully In Ground"])
	
	voxels._feature_dict["Small " + _name + " Patch"] = small_dirt_patch
	_features["Small Dirt Patch"] = small_dirt_patch
