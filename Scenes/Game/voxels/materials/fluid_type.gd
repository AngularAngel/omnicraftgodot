extends "res://Scenes/Game/voxels/materials/voxel_family.gd"

func initialize(voxels):
	var water = voxels.addFluidVoxel(_name, self)
	
	var _material = voxels._shaders.get_new_shader_material(_name, voxels._shaders._water_shader);
		
	water.set_material_override(0, _material);
	
	var immersion_material = voxels._shaders.get_new_shader_material(_name, voxels._shaders._water_immersion_shader);
	
	water.set_meta("FluidImmersion", immersion_material);
