extends Object

const SET =          0x0
const ADD =          0x1
const SUB =          0x2
const MULT =         0x3
const DIV =          0x4
const MIX =          0x5
const THRESHOLDSET = 0x6
const ALPHAADD =     0x7
const ALPHASUB =     0x8

const COLOR_CHANNEL =       0x0F00;
const COLOR_RGB_CHANNEL =   0x0E00;
const COLOR_R_CHANNEL =     0x0800;
const COLOR_G_CHANNEL =     0x0400;
const COLOR_B_CHANNEL =     0x0200;
const COLOR_A_CHANNEL =     0x0100;

#These are out of order to work around colors, above.
const TEMP_1_CHANNEL =      0x4000;
const TEMP_2_CHANNEL =      0x2000;
const NORMAL_CHANNEL =      0x1000;

const METALLICITY_CHANNEL = 0x0080;
const SPECULAR_CHANNEL =    0x0040;
const ROUGHNESS_CHANNEL =   0x0020;
const LIGHT_CHANNEL =       0x0010;

const POSITION_CHANNEL =    0x0008;
const UV_CHANNEL =          0x0004;
const TEXTURE_ID_CHANNEL =  0x0002;
	
const FINAL =              0x0001;

const SOLID =                 0
const RANDOMLIST =            1
const CHECKERBOARD =          2
const VALUENOISE =            3
const RANDOMRANGE =           4
const MLSIMPLEX =             5
const SETTORANGE =            6
const COPYOP =                  7
const MULTIPLYCOLOR =         9
const EDGELAYERING =          10
const GAUSSIANRANGE =         11
const THRESHOLD =             12
const FROMLIST =              13
const TRANSPARENCY =          14



const HORIZONTAL_EDGE =         0.0;
const HORIZONTAL_EDGE_SQUARED = 1.0;
const VERTICAL_EDGE =           2.0;
const VERTICAL_EDGE_SQUARED =   3.0;
const NOISE_EDGE =              4.0;
const SMOOTH_NOISE_EDGE =       5.0;
const OUTER_SMOOTH_NOISE_EDGE = 6.0;


func combine(patterns: Array):
	var result = Array();
	for pattern in patterns:
		result.append_array(pattern);
	
	return result;


func solid(applicationParams : Color, color : Color):
		return [applicationParams, Color(SOLID, 0, 0, 1), color];


func randomList(applicationParams : Color, colors: Array):
	var result = [applicationParams, Color(RANDOMLIST, 0, 0, colors.size())];
	result.append_array(colors);
	return result;


func sqCheckerboard(applicationParams : Color, size: int, colors: Array):
	var result = [applicationParams, Color(CHECKERBOARD, size, size, colors.size()),];
	result.append_array(colors);
	return result;


func valueNoise(applicationParams : Color, intensity : float, smooth_interp := true, noise_seed := Color(0, 0, 0, 0)):
		return [applicationParams, Color(VALUENOISE, intensity, 1.0 if smooth_interp else 0.0, 1), noise_seed]


func randomRange(applicationParams : Color, rangeValue : float):
	return [applicationParams, Color(RANDOMRANGE, 0, 0, rangeValue),]


func gaussianRange(applicationParams : Color, rangeValue : float):
	return [applicationParams, Color(GAUSSIANRANGE, 0, 0, rangeValue),]


func mlSimplex(applicationParams : Color, brightnessOffset : float, layerNoises : Array):
	var result = [applicationParams, Color(MLSIMPLEX, layerNoises.size(), brightnessOffset, 0),]
	result.append_array(layerNoises);
	return result


func setToRange(applicationParams : Color, range_min : float, range_max : float, source := TEMP_1_CHANNEL):
	var result = [applicationParams, Color(SETTORANGE, source, range_min, range_max),]
	return result


func copyOp(applicationParams : Color, source := TEMP_1_CHANNEL):
	var result = [applicationParams, Color(COPYOP, source, 0, 0),]
	return result


func multiplyColor(applicationParams : Color, color: Color, source := TEMP_1_CHANNEL):
	return [applicationParams, Color(MULTIPLYCOLOR, source, 0, 1), color];


func edgeLayering(applicationParams : Color, edgeLayers : Array):
	var result = [applicationParams, Color(EDGELAYERING, edgeLayers.size(), 0, 0),]
	result.append_array(edgeLayers);
	return result


func threshold(applicationParams : Color, threshold_value: float, color : Color, source := TEMP_1_CHANNEL):
		return [applicationParams, Color(THRESHOLD, source, threshold_value, 1), color];


func fromList(applicationParams : Color, colors: Array, source := TEMP_1_CHANNEL):
	var result = [applicationParams, Color(FROMLIST, source, 0, colors.size())];
	result.append_array(colors);
	return result;


func transparency(applicationParams : Color, color : Color, source := TEMP_1_CHANNEL):
	return [applicationParams, Color(TRANSPARENCY, source, 0, 1), color];
