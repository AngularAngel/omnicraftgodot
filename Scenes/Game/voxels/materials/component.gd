extends "res://Scenes/Game/voxels/materials/constants.gd"

var albedo: Color;
var light := Color(0, 0, 0, 0);
var specular := 0.0;
var metallicity := 0.0;
var roughness := 0.0;


func get_value(channel):
	match(channel):
		(COLOR_CHANNEL):
			return albedo;
		(LIGHT_CHANNEL):
			return light;
		(SPECULAR_CHANNEL):
			return Color(specular, 0, 0, 0);
		(METALLICITY_CHANNEL):
			return Color(metallicity, 0, 0, 0);
		(ROUGHNESS_CHANNEL):
			return Color(roughness, 0, 0, 0);
