extends "res://Scenes/Game/voxels/materials/constants.gd"


const _CHANNELS = [COLOR_CHANNEL, LIGHT_CHANNEL, SPECULAR_CHANNEL, METALLICITY_CHANNEL, ROUGHNESS_CHANNEL];

var name:String;

var _components = [];
var _percentages = [];


func add_component(component, percentage):
	_components.append(component);
	_percentages.append(percentage);


func get_values(channel):
	var values = [];
	for component in _components:
		values.append(component.get_value(channel));
	
	return values;

func generate_list(channel):
	var values = [];
	var empty = true;
	for component in _components:
		var value = component.get_value(channel);
		values.append(value);
		if (!is_instance_of(value, TYPE_COLOR) || value != Color(0, 0, 0, 0)) && (!is_instance_of(value, TYPE_FLOAT) || value != 0.0):
			empty = false;
	
	if not empty:
		return fromList(Color(TEMP_1_CHANNEL, 0, channel, SET), values);
	else:
		return [];


func generate_component_lists():
	var values = [];
	for channel in _CHANNELS:
		values.append_array(generate_list(channel));
	
	return values;

func generate_overlay_lists():
	var values = [];
	
	for component in _components:
		values.append_array(valueNoise(Color(POSITION_CHANNEL, UV_CHANNEL, TEMP_1_CHANNEL, SET), -0.95, true, component.albedo))
		values.append_array(transparency(Color(TEMP_1_CHANNEL, 0, COLOR_RGB_CHANNEL, MIX), component.albedo));
		values.append_array(transparency(Color(TEMP_1_CHANNEL, 0, SPECULAR_CHANNEL, MIX), Color(component.specular, 0, 0, 0)));
		values.append_array(transparency(Color(TEMP_1_CHANNEL, 0, ROUGHNESS_CHANNEL, MIX), Color(component.roughness, 0, 0, 0)));
	return values;


func set_shader_material_parameters(shader_material):
	var colors = [];
		
	for color in get_values(COLOR_CHANNEL):
		colors.append(Vector3(color.r, color.g, color.b))
	
	shader_material.set_shader_parameter("colors", colors);
	
	var speculars = [];
	
	for specular in get_values(SPECULAR_CHANNEL):
		speculars.append(specular.r)
	
	shader_material.set_shader_parameter("speculars", speculars)
	
	var roughnesses = [];
	
	for roughness in get_values(ROUGHNESS_CHANNEL):
		roughnesses.append(roughness.r)
	
	shader_material.set_shader_parameter("roughnesses", roughnesses)
