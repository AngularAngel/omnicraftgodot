extends "res://Scenes/Game/voxels/features/homogenous_multiblock_feature.gd"

var _multiblock_x;
var _multiblock_y;
var _multiblock_z;

func place(out_buffer: VoxelBuffer, position: Vector3i):
	for x in range(_dimensions.x):
		for y in range(_dimensions.y):
			for z in range(_dimensions.z):
				var id = _block_id;
				if x > 0:
					id = _multiblock_x;
				elif z > 0:
					id = _multiblock_z;
				elif y > 0:
					id = _multiblock_y;
					
				out_buffer.set_voxel(id, position.x + x, position.y + y, position.z + z, _Block_Channel)

func copy(copy = null):
	if copy == null:
		copy = duplicate(DUPLICATE_SCRIPTS)
		
	copy._multiblock_x = _multiblock_x;
	copy._multiblock_y = _multiblock_y;
	copy._multiblock_z = _multiblock_z;
	return super.copy(copy)
