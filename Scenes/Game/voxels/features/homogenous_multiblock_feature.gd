extends "res://Scenes/Game/voxels/features/block_feature.gd"

var _dimensions : Vector3i


func get_dimensions():
	return _dimensions;

func place(out_buffer: VoxelBuffer, position: Vector3i):
	for x in range(_dimensions.x):
		for y in range(_dimensions.y):
			for z in range(_dimensions.z):
				out_buffer.set_voxel(_block_id, position.x + x, position.y + y, position.z + z, _Block_Channel)

func copy(copy = null):
	if copy == null:
		copy = duplicate(DUPLICATE_SCRIPTS)
		
	copy._dimensions = Vector3i(_dimensions);
	return super.copy(copy)
