extends Node

const Structure = preload("./structure.gd")

var _conditions = []
var _placement_modifiers = []

func get_dimensions():
	return Vector3i(1, 1, 1)

func get_position(generator, position: Vector3i, random: RandomNumberGenerator):
	for placement_modifier in _placement_modifiers:
		position = placement_modifier.call(self, generator, position, random);
	return position
	
func check(generator, position: Vector3i, random: RandomNumberGenerator):
	for condition in _conditions:
		if !condition.call(self, generator, position, random):
			return false;
	return true
	
func place(_out_buffer: VoxelBuffer, _position: Vector3i):
	pass;
	
func get_structure(_position: Vector3i):
	var structure := Structure.new()
	structure.offset = _position;
	var dimensions = get_dimensions()
	structure.voxels.create(dimensions.x, dimensions.y, dimensions.z)
	place(structure.voxels, Vector3i(0, 0, 0))
	return structure
	
func copy(copy = null):
	if copy == null:
		copy = duplicate(DUPLICATE_SCRIPTS)
		
	for condition in _conditions:
		copy._conditions.append(condition);
		
	for placement_modifier in _placement_modifiers:
		copy._placement_modifiers.append(placement_modifier);
	
	return copy;
