extends "res://Scenes/Game/voxels/features/feature.gd"

const _Block_Channel = VoxelBuffer.CHANNEL_TYPE;

var _block_id;

func place(out_buffer: VoxelBuffer, position: Vector3i):
	out_buffer.set_voxel(_block_id, position.x, position.y, position.z, _Block_Channel)

	
func copy(copy = null):
	if copy == null:
		copy = duplicate(DUPLICATE_SCRIPTS)
		
	copy._block_id = _block_id;
	return super.copy(copy)
