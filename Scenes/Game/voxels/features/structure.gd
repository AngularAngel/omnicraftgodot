extends Node

var offset := Vector3i()
var voxels := VoxelBuffer.new()

func get_dimensions():
	return voxels.get_size();
