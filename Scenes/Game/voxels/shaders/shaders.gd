extends Object

var _vegetation_shader: Shader = preload("./vegetation.gdshader")
var _earth_shader: Shader = preload("./earth.gdshader")
var _glass_shader: Shader = preload("./glass.gdshader")
var _water_shader: Shader = preload("./water.gdshader")
var _top_grass_shader: Shader = preload("./top_grass.gdshader")
var _side_grass_shader: Shader = preload("./side_grass.gdshader")
var _water_immersion_shader: Shader = preload("./water_immersion.gdshader")
var _berry_shader: Shader = preload("./berry.gdshader")


var _material_array = []
var _material_dict = {}


func add_material(material_name : String, material : Material):
	var id = _material_array.size()
	_material_array.append(material)
	_material_dict[material_name] = id;
	return id;


func get_new_shader_material(material_name : String, shader: Shader):
	var shaderMaterial := ShaderMaterial.new()
	add_material(material_name, shaderMaterial)
	shaderMaterial.set_shader(shader)
	return shaderMaterial
