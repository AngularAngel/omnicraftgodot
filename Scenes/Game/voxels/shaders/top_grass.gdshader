shader_type spatial;
render_mode blend_mix, depth_draw_opaque, diffuse_burley, specular_schlick_ggx, cull_back;

#include "res://Imports/surface.gdshaderinc"

uniform vec3 plant_color = vec3(0, 0.25, 0);

void vertex() {
	VERTEX += NORMAL * 0.005;
	UV = UV * uv1_scale.xy + uv1_offset.xy;
}

void fragment() {
	vec3 world_pos = (INV_VIEW_MATRIX * vec4(VERTEX, 1.0)).xyz;
	
	vec3 texel_pos = world_pos * texels_per_block;
	ivec2 texel_uv = ivec2(floor(UV * texels_per_block));
	
	vec3 world_normal = (INV_VIEW_MATRIX * vec4(NORMAL, 0.0)).xyz;
	
	if (!FRONT_FACING)
		world_normal = -world_normal;
		
	//Translate normal back to world coordinates, then add it to texel pos so that we don't get z-fighting on the sides.
	texel_pos -= world_normal * 0.25; 
	texel_pos = floor(texel_pos);
	
	
	
	if (!FRONT_FACING)
		world_normal = -world_normal;
		
	float seed = getSeed(texel_pos);
    float noise = valueNoise(vec2(texel_uv) / 2.0, seed);
	float n = smoothInterp(noise);
    n = fuzzyor(n, 196.0 / 255.0);
    
	ALBEDO = pow(COLOR.rgb, vec3(2.0)) * plant_color * n;
	
	n = smoothInterp(noise);
    n = mix(0.75, 1.0, n);
	
	ALPHA = n;
	
	float variation = 0.125;
	world_normal += vec3(gaussianRandomF(seed * 0.23, 6) * variation * 2.0 - variation,
                          gaussianRandomF(seed, 6) * variation * 2.0 - variation,
                          gaussianRandomF(seed * 0.564, 6) * variation * 2.0 - variation);
	
	NORMAL = (VIEW_MATRIX * vec4(world_normal, 0.0)).xyz;
	
	n = smoothInterp(noise);
    n = fuzzyor(n, 155.0 / 255.0);
	
	SPECULAR = clamp(0.05125 * n, 0.0001, 0.9999);
	CLEARCOAT = SPECULAR;
	
	n = smoothInterp(noise);
    n = fuzzyor(n, 196.0 / 255.0);
	
	ROUGHNESS = clamp((196.0 / 255.0) * n, 0.01, 0.9999);
	CLEARCOAT_ROUGHNESS = ROUGHNESS;
}