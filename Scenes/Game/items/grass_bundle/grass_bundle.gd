extends ItemType


# Called when the node enters the scene tree for the first time.
func initialize_item(item: Item):
	item = super.initialize_item(item)
	var item_db : ItemDB = get_parent();
	
	var bundle = item_db._component_registry.get_entry_by_name("bundle").get_component()
	item.add_child(bundle);
	
	var cut_grass = item_db._material_registry.get_entry_by_name("cut_grass").get_material()
	bundle.add_child(cut_grass);
	
	return item;
