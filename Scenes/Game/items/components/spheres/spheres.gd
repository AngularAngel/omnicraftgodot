extends ItemComponentType

func initialize_component(component):
	component._ready_lambda = func(component):
		var item = component.get_parent();
		
		component.set_meta("radius", 1.0);
		component.set_meta("quantity", 1.0);
		component.set_meta("volume", 1.0 * component.get_meta("radius") * component.get_meta("quantity"));
		item.set_meta("volume", component.get_meta("volume"));
		item.set_meta("spriteScale", 0.25 * component.get_meta("radius"));
