extends Node

class_name ItemComponent

var _ready_lambda : Callable;


func _ready():
	if _ready_lambda != null:
		_ready_lambda.call(self)
