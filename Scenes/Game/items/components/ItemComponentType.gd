extends Datum

class_name ItemComponentType


var component_script := ItemComponent;


func get_component() -> ItemComponent:
	var component := component_script.new();
	initialize_component(component);
	
	return component;


func initialize_component(component: ItemComponent):
	return component;
