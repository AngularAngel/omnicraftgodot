extends ItemType;


# Called when the node enters the scene tree for the first time.
func initialize_item(item: Item):
	item = super.initialize_item(item)
	var item_db : ItemDB = get_parent();
	
	var basket = item_db._component_registry.get_entry_by_name("basket").get_component()
	item.add_child(basket);
	
	var basket_handle = item_db._component_registry.get_entry_by_name("basket_handle").get_component()
	item.add_child(basket_handle);
	
	var cut_grass_material_type = item_db._material_registry.get_entry_by_name("cut_grass")
	basket.add_child(cut_grass_material_type.get_material());
	basket_handle.add_child(cut_grass_material_type.get_material());
	
	return item;
