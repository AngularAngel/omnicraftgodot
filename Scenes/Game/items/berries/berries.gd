extends ItemType;


# Called when the node enters the scene tree for the first time.
func initialize_item(item: Item):
	item = super.initialize_item(item)
	var item_db : ItemDB = get_parent();
	
	var spheres = item_db._component_registry.get_entry_by_name("spheres").get_component()
	item.add_child(spheres);
	
	var berry = item_db._material_registry.get_entry_by_name("berry").get_material()
	spheres.add_child(berry);
	
	return item;
