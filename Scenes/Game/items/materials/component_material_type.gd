extends Datum

class_name ComponentMaterialType


var material_script := ComponentMaterial;


func get_material() -> ComponentMaterial:
	var material := material_script.new();
	initialize_material(material);
	
	return material;


func initialize_material(material: ComponentMaterial):
	return material;
