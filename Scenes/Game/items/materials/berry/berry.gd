extends ComponentMaterialType


func initialize_material(material):
	material._ready_lambda = func(material):
		var item = material.get_parent().get_parent();
		var component = material.get_parent()
		
		await component.ready;
		
		component.set_meta("calories", 100.0 * component.get_meta("volume", 1));
		item.set_meta("calories", component.get_meta("calories") + item.get_meta("calories", 0));
