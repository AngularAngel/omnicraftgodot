extends ComponentMaterialType

func initialize_material(material):
	material._ready_lambda = func(material):
		var item = material.get_parent().get_parent();
		var component = material.get_parent()
		
		material.set_meta("tensile_strength", 1);
		
		#await component.ready;
