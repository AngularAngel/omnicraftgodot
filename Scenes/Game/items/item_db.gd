extends DatumRegistry

class_name ItemDB


const ITEM_ROOT = "res://Scenes/Game/items/"
const COMPONENT_ROOT = "res://Scenes/Game/items/components"
const MATERIAL_ROOT = "res://Scenes/Game/items/materials"

var _component_registry = DatumRegistry.new();
var _material_registry = DatumRegistry.new();


func _init():
	_material_registry._root_folder = MATERIAL_ROOT;
	add_child(_material_registry)
	
	_material_registry.create_and_add_entry("berry")
	_material_registry.create_and_add_entry("cut_grass")
	
	_component_registry._root_folder = COMPONENT_ROOT;
	add_child(_component_registry)
	
	_component_registry.create_and_add_entry("spheres")
	_component_registry.create_and_add_entry("bundle")
	_component_registry.create_and_add_entry("basket")
	_component_registry.create_and_add_entry("basket_handle")
	
	create_and_add_item("grass_bundle")
	create_and_add_item("berries")
	create_and_add_item("basket")


func create_and_add_item(name: String):
	var dir = str(ITEM_ROOT, "/", name, "/")
	create_and_add_entry(name, null, dir);


func create_entry(name: String, behavior_script = null, dir = null) -> ItemType:
	if behavior_script == null:
		behavior_script = get_behavior_script(name, dir);
	
	var item : ItemType = behavior_script.new();
	
	# Give the node a deterministic name for networking
	item.name = name
	item._display_name = name.capitalize()
	item._id = len(_entries)
	item._sprite = load(str(dir, name, "_sprite.png"))
	return item;

