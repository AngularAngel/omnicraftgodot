extends Datum

class_name ItemType


var _sprite : Texture;


func get_item() -> Item:
	var item := Item.new();
	item = initialize_item(item);
	get_node("../../Items").add_child(item)
	
	return item;


func initialize_item(item: Item):
	item._sprite = _sprite;
	return item;
