extends DatumRegistry

class_name InteractionDB


const ROOT = "res://Scenes/Game/interaction_methods"
const VOXEL_INTERACTIONS = ROOT + "/world/voxels"
const ENTITY_INTERACTIONS = ROOT + "/world/entities"
const ITEM_INTERACTIONS = ROOT + "/self/items"
const ACTIVITIES = ROOT + "/self/activities"


func _init():
	create_and_add_block_interaction("trample")
	create_and_add_block_interaction("harvest")
	create_and_add_entity_interaction("pick_up")
	create_and_add_entity_interaction("put")
	create_and_add_item_interaction("eat")
	create_and_add_item_interaction("put_down")
	create_and_add_activity("crafting")


func create_and_add_block_interaction(interaction_name: String):
	var dir = str(VOXEL_INTERACTIONS, "/", interaction_name, "/")
	create_and_add_entry(interaction_name, null, dir);


func create_and_add_entity_interaction(interaction_name: String):
	var dir = str(ENTITY_INTERACTIONS, "/", interaction_name, "/")
	create_and_add_entry(interaction_name, null, dir);


func create_and_add_item_interaction(interaction_name: String):
	var dir = str(ITEM_INTERACTIONS, "/", interaction_name, "/")
	create_and_add_entry(interaction_name, null, dir);


func create_and_add_activity(interaction_name: String):
	var dir = str(ACTIVITIES, "/", interaction_name, "/")
	create_and_add_entry(interaction_name, null, dir);


func create_entry(interaction_name: String, behavior_script = null, dir = null) -> InteractionMethod:
	if behavior_script == null:
		behavior_script = get_behavior_script(interaction_name, dir);
	
	var interaction : InteractionMethod = behavior_script.new();
	
	# Give the node a deterministic name for networking
	interaction.name = interaction_name
	interaction._display_name = interaction_name.capitalize()
	interaction._id = len(_entries)
	interaction._sprite = load(str(dir, interaction_name, "_sprite.png"))
	return interaction;
