extends Activity


func can_craft_item(item_type: ItemType):
	return true;


func craft_item(item_type: ItemType):
	_player._inventory.accept_item(item_type.get_item());


func has_options() -> bool:
	return _get_craftable_items().size() > 0;


func show_options(interaction_menu):
	var craftable_items = _get_craftable_items();
	for item_type in craftable_items:
		var option = interaction_menu.add_option(item_type, craft_item.bind(item_type));


func _get_craftable_items():
	var craftable_items = [];
	for item_type in _player._knowledge.craftable_items:
		if can_craft_item(item_type):
			craftable_items.append(item_type);
	return craftable_items;
