extends SelfInteraction


class_name ItemInteraction


func can_be_used_on(_item) -> bool:
	return true;

func interact(_inventory_slot):
	pass;


func has_options() -> bool:
	return _get_applicable_inventory_slots().size() > 0;


func show_options(interaction_menu):
	var inventory_slots = _get_applicable_inventory_slots();
	for inventory_slot in inventory_slots:
		var option = interaction_menu.add_option(inventory_slot._item, interact.bind(inventory_slot));
		
		var item_changed = func item_changed():
			if inventory_slot._item != null and can_be_used_on(inventory_slot._item):
				option.set_target(inventory_slot._item);
			else:
				option.queue_free();
				if interaction_menu._container.get_children().size() <= 1:
					interaction_menu.clear();
		
		inventory_slot.item_changed.connect(item_changed);
		option.tree_exiting.connect(func(): inventory_slot.item_changed.disconnect(item_changed));


func _get_applicable_inventory_slots():
	var inventory_slots = [];
	for inventory_slot in _player._inventory.get_inventory_slots():
		if can_be_used_on(inventory_slot._item):
			inventory_slots.append(inventory_slot);
	return inventory_slots;
