extends ItemInteraction


func can_be_used_on(item):
	return item.get_meta("calories", 0.0) > 0.0;

func interact(inventory_slot):
	inventory_slot._item.queue_free();
	inventory_slot.set_item(null);
