extends ItemInteraction


var put_interaction : EntityInteraction;


func initialize(world_interaction):
	super.initialize(world_interaction)
	put_interaction = get_node("/root/Main/Game/Voxels/Interactions").get_entry_by_name("put");


func interact(inventory_slot):
	var entity_interaction = _player.get_node("HUD/Interaction/EntityInteraction");
	entity_interaction.activate_interaction(put_interaction)
	put_interaction._inventory_slot = inventory_slot;
	entity_interaction._interaction_menu.clear();
