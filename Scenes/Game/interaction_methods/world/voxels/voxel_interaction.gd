extends WorldInteraction

class_name VoxelInteraction


var _terrain_tool : VoxelTool;

func initialize(world_interaction):
	super.initialize(world_interaction)
	_terrain_tool = world_interaction._terrain_tool;

func can_be_used_on(voxel :VoxelBlockyModel):
	return true;


func is_within_range(voxel: VoxelBlockyModel, voxel_position : Vector3i, hit_position : Vector3i) -> bool:
	var distance = Vector3(hit_position)
	distance.x += 0.5;
	distance.y += 0.5;
	distance.z += 0.5;
	distance -= _player.get_position();
	return distance.length() <= _range;


func is_allowed(voxel: VoxelBlockyModel, voxel_position : Vector3i, hit_position : Vector3i) -> bool:
	return is_within_range(voxel, voxel_position, hit_position);

func interact(_voxel: VoxelBlockyModel, _voxel_position : Vector3i, _hit_position : Vector3i):
	pass
