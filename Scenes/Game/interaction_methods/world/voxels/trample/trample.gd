extends VoxelInteraction

func initialize(world_interaction):
	super.initialize(world_interaction)
	_range = 1.5;

func can_be_used_on(voxel :VoxelBlockyModel):
	var voxel_family = voxel.get_meta("voxelFamily");
	if voxel_family == null:
		return false;
	var family_name :String = voxel_family._name;
	return family_name.begins_with("Bush") or family_name.begins_with("Grass");


func interact(voxel: VoxelBlockyModel, voxel_position : Vector3i, hit_position : Vector3i):
	var size = Vector3i(voxel.get_meta("selectionSize", Vector3(1, 1, 1)).ceil())
	for x in range(size.x):
		for y in range(size.y):
			for z in range(size.z):
				var set_position = Vector3i(voxel_position)
				set_position.x += x;
				set_position.y += y;
				set_position.z += z;
				_terrain_tool.set_voxel(set_position, 0)
