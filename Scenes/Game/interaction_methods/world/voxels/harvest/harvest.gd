extends VoxelInteraction


func initialize(world_interaction):
	super.initialize(world_interaction)


func can_be_used_on(voxel :VoxelBlockyModel):
	return voxel.get_meta("isHarvestable", false);


func remove_block(voxel: VoxelBlockyModel, voxel_position : Vector3i):
	var size = Vector3i(voxel.get_meta("selectionSize", Vector3(1, 1, 1)).ceil())
	var block = voxel.get_meta("harvestBlock", null)
	var block_id = 0;
	if block != null:
		block_id = block.get_meta("ID");
	for x in range(size.x):
		for y in range(size.y):
			for z in range(size.z):
				var set_position = Vector3i(voxel_position)
				set_position.x += x;
				set_position.y += y;
				set_position.z += z;
				_terrain_tool.set_voxel(set_position, block_id)


func get_item(voxel: VoxelBlockyModel):
	_player._inventory.accept_item(voxel.get_meta("harvestItem").get_item())


func interact(voxel: VoxelBlockyModel, voxel_position : Vector3i, hit_position : Vector3i):
	remove_block(voxel, voxel_position)
	get_item(voxel);
