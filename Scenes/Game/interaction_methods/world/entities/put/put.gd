extends EntityInteraction

const ItemOnGround = preload("res://Scenes/Entities/Items/ItemOnGround.tscn")


var _inventory_slot;


func can_be_used_on(entity):
	return entity.get_meta("isTerrain", false) && _inventory_slot._item != null;


func interact(entity):
	var item_on_ground = ItemOnGround.instantiate();
	item_on_ground._terrain = _player._terrain;
	item_on_ground.position = (_player._raycast.get_collision_point() + 
			_player._raycast.get_collision_normal() * 0.1)
	entity.get_parent().add_child(item_on_ground)
	item_on_ground.set_item(_inventory_slot._item);
	_inventory_slot.set_item(null);
	
	_player.get_node("HUD/Interaction/EntityInteraction")._active_interactions.erase(self)
