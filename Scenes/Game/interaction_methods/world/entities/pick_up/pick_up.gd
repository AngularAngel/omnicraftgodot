extends EntityInteraction


func can_be_used_on(entity):
	return entity.get_meta("isItem", false);


func is_allowed(entity):
	return _player._inventory.can_accept_item(entity._item)


func interact(entity):
	_player._inventory.accept_item(entity._item)
	entity.queue_free()
