extends Datum

class_name InteractionMethod


var _sprite : Texture
var _player;


func initialize(interaction_control):
	_player = interaction_control.get_parent().get_parent().get_parent();


func can_be_used_by(player):
	return true;
