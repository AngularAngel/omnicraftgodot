extends "res://Scenes/Game/floating_island/voxel_fillers/voxel_filler.gd"

var _water_id: int;

var _water_height := 0;

func initialize_voxels(_Voxels):
	_water_id = _Voxels._voxel_library.get_model_index_from_resource_name("Water")


func fill_voxels(out_buffer: VoxelBuffer, bufferSize: Vector3i, origin_in_voxels: Vector3i, gx: int, gz: int, x: int, z: int):
	if _heightmap_generator.voxel_within_bounds(gx, gz, -1):
		
		if origin_in_voxels.y > _water_height:
			return;
		
		var ground_height = _heightmap_generator._get_height_at(gx, gz) - 1;
		
		if ground_height >= _water_height:
			return;
		
		var relative_ground_height = ground_height - origin_in_voxels.y;
		
		if relative_ground_height > bufferSize.y:
			return;
		
		var relative_water_height = _water_height - origin_in_voxels.y;
		
		out_buffer.fill_area(_water_id,
				Vector3(x, relative_ground_height, z), Vector3(x + 1, relative_water_height, z + 1), _Block_Channel)
