extends "res://Scenes/Game/floating_island/voxel_fillers/voxel_filler.gd"

var glass;

func initialize_voxels(_Voxels):
	glass = _Voxels._voxel_library.get_model_index_from_resource_name("Glass")


func fill_voxels(out_buffer: VoxelBuffer, bufferSize: Vector3i, _origin_in_voxels: Vector3i, gx: int, gz: int, x: int, z: int):
	if not _heightmap_generator.voxel_within_bounds(gx, gz, -1):
		out_buffer.fill_area(glass, Vector3(x, 0, z), Vector3(x + 1, bufferSize.y, z + 1), _Block_Channel)
