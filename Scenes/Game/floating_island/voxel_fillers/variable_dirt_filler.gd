extends "res://Scenes/Game/floating_island/voxel_fillers/dirt_filler.gd"

var _height_source;


func fill_voxels(out_buffer: VoxelBuffer, bufferSize: Vector3i, origin_in_voxels: Vector3i, gx: int, gz: int, x: int, z: int):
	if _heightmap_generator.voxel_within_bounds(gx, gz, -1):
		var height = _heightmap_generator._get_height_at(gx, gz);
		var relative_height = height - origin_in_voxels.y;
		
		var dirt_height = _height_source._get_height_at(gx, gz);
		if relative_height > 1 && dirt_height > 0:
			for cur_height in range(dirt_height):
				fill_dirt(out_buffer, bufferSize, x, relative_height - (1 + cur_height), z);
