extends "res://Scenes/Game/floating_island/voxel_fillers/voxel_filler.gd"

var stone = null;

func initialize_voxels(_Voxels):
	if stone == null:
		push_error("Stone id not yet set!")


func fill_voxels(out_buffer: VoxelBuffer, bufferSize: Vector3i, origin_in_voxels: Vector3i, gx: int, gz: int, x: int, z: int):
	if _heightmap_generator.voxel_within_bounds(gx, gz, -1):
		var height = _heightmap_generator._get_height_at(gx, gz);
		var relative_height = height - origin_in_voxels.y;
		
		# stone and dirt
		if relative_height > bufferSize.y + 1:
			out_buffer.fill_area(stone,
				Vector3(x, 0, z), Vector3(x + 1, bufferSize.y, z + 1), _Block_Channel)
		elif relative_height > 1:
				out_buffer.fill_area(stone,
					Vector3(x, 0, z), Vector3(x + 1, relative_height - 1, z + 1), _Block_Channel)
