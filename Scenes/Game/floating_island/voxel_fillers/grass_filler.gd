extends "res://Scenes/Game/floating_island/voxel_fillers/voxel_filler.gd"

var grass_side;
var downwards_grass_side;
var _voxels

var _minimum_grass_height := 0;

func initialize_voxels(_Voxels):
	_voxels = _Voxels
	if grass_side == null || downwards_grass_side == null:
		push_error("Grass side ids not yet set!")


func fill_voxels(out_buffer: VoxelBuffer, bufferSize: Vector3i, origin_in_voxels: Vector3i, gx: int, gz: int, x: int, z: int):
	if _heightmap_generator.voxel_within_bounds(gx, gz, -1):
		var height = _heightmap_generator._get_height_at(gx, gz);
		if height < _minimum_grass_height:
			return;
		var relative_height = height - origin_in_voxels.y;
		
		if relative_height > 1 && relative_height < bufferSize.y + 2 && \
				_voxels._voxel_library.get_model(out_buffer.get_voxel(x, relative_height - 2, z, _Block_Channel)).get_meta("Is_Dirt", false):
			out_buffer.set_voxel(grass_side, x, relative_height - 2, z, _Top_Side_Channel)
			if height > _minimum_grass_height:
				if _heightmap_generator._get_height_at(gx + 1, gz) < height:
					out_buffer.set_voxel(downwards_grass_side, x, relative_height - 2, z, _Right_Side_Channel)
				if _heightmap_generator._get_height_at(gx - 1, gz) < height:
					out_buffer.set_voxel(downwards_grass_side, x, relative_height - 2, z, _Left_Side_Channel)
				if _heightmap_generator._get_height_at(gx, gz + 1) < height:
					out_buffer.set_voxel(downwards_grass_side, x, relative_height - 2, z, _Back_Side_Channel)
				if _heightmap_generator._get_height_at(gx, gz - 1) < height:
					out_buffer.set_voxel(downwards_grass_side, x, relative_height - 2, z, _Front_Side_Channel)
