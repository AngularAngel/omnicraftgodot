extends Node

const _Block_Channel = VoxelBuffer.CHANNEL_TYPE;
const _Top_Side_Channel = VoxelBuffer.CHANNEL_SDF;
#Out of order here, cause the color channel actually does something.
const _Bottom_Side_Channel = VoxelBuffer.CHANNEL_DATA7;
const _Front_Side_Channel = VoxelBuffer.CHANNEL_INDICES;
const _Back_Side_Channel = VoxelBuffer.CHANNEL_WEIGHTS;
const _Left_Side_Channel = VoxelBuffer.CHANNEL_DATA5;
const _Right_Side_Channel = VoxelBuffer.CHANNEL_DATA6;

var _heightmap_generator;

func initialize(floating_island):
	_heightmap_generator = floating_island
	initialize_voxels(floating_island.get_voxels())


func initialize_voxels(_Voxels):
	pass;


func fill_voxels(_out_buffer: VoxelBuffer, _bufferSize: Vector3i, _origin_in_voxels: Vector3i, _gx: int, _gz: int, _x: int, _z: int):
	pass;
