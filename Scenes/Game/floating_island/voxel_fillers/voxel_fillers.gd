extends Node

const Glass_Filler = preload("./glass_filler.gd")
const Stone_Filler = preload("./stone_filler.gd")
const Dirt_Filler = preload("./dirt_filler.gd")
const Variable_Dirt_Filler = preload("./variable_dirt_filler.gd")
const Grass_Filler = preload("./grass_filler.gd")
const Water_Filler = preload("./water_filler.gd")


var dict = {}

func initialize():
	dict["Glass Filler"] = Glass_Filler;
	dict["Stone Filler"] = Stone_Filler;
	dict["Dirt Filler"] = Dirt_Filler;
	dict["Variable Dirt Filler"] = Variable_Dirt_Filler;
	dict["Grass Filler"] = Grass_Filler;
	dict["Water Filler"] = Water_Filler;


func get_voxel_filler(voxel_name):
	var voxel_filler = dict[voxel_name].new();
	voxel_filler.set_name(voxel_name);
	return voxel_filler
