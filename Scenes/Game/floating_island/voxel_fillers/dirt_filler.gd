extends "res://Scenes/Game/floating_island/voxel_fillers/voxel_filler.gd"

var air;
var dirt;

func initialize_voxels(_Voxels):
	air = _Voxels._voxel_library.get_model_index_from_resource_name("Air")
	if dirt == null:
		push_error("Dirt id not yet set!")


func fill_dirt(out_buffer: VoxelBuffer, bufferSize: Vector3i, x, y, z):
	if y >= 0 && y < bufferSize.y && out_buffer.get_voxel(x, y, z, _Block_Channel) != air:
		out_buffer.set_voxel(dirt, x, y, z, _Block_Channel)


func fill_voxels(out_buffer: VoxelBuffer, bufferSize: Vector3i, origin_in_voxels: Vector3i, gx: int, gz: int, x: int, z: int):
	if _heightmap_generator.voxel_within_bounds(gx, gz, -1):
		var height = _heightmap_generator._get_height_at(gx, gz);
		var relative_height = height - origin_in_voxels.y;
		
		if relative_height > 1 && relative_height < bufferSize.y + 2:
			fill_dirt(out_buffer, bufferSize, x, relative_height - 2, z);
