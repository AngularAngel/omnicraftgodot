extends VoxelGeneratorScript

const _Top_Side_Channel = VoxelBuffer.CHANNEL_SDF;
const _Bottom_Side_Channel = VoxelBuffer.CHANNEL_DATA7;
const _Front_Side_Channel = VoxelBuffer.CHANNEL_INDICES;
const _Back_Side_Channel = VoxelBuffer.CHANNEL_WEIGHTS;
const _Left_Side_Channel = VoxelBuffer.CHANNEL_DATA5;
const _Right_Side_Channel = VoxelBuffer.CHANNEL_DATA6;

var _floating_island;

func prep_channel(out_buffer, channel):
	out_buffer.set_channel_depth(channel, VoxelBuffer.DEPTH_32_BIT)
	out_buffer.fill(0, channel)

func _generate_block(out_buffer: VoxelBuffer, origin_in_voxels: Vector3i, _lod: int):
	var gx := origin_in_voxels.x
	var gz := origin_in_voxels.z
		
	prep_channel(out_buffer, _Top_Side_Channel);
	prep_channel(out_buffer, _Bottom_Side_Channel);
	prep_channel(out_buffer, _Front_Side_Channel);
	prep_channel(out_buffer, _Back_Side_Channel);
	prep_channel(out_buffer, _Left_Side_Channel);
	prep_channel(out_buffer, _Right_Side_Channel);
	
	if not _floating_island.block_within_bounds(gx, gz):
		return;
	
	_floating_island.fill_voxels(out_buffer, origin_in_voxels)
	
	_floating_island.fill_structures(out_buffer, origin_in_voxels)
