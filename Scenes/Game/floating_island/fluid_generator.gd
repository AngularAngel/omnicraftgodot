extends VoxelGeneratorScript


const _Top_Side_Channel = VoxelBuffer.CHANNEL_SDF;
const _Front_Side_Channel = VoxelBuffer.CHANNEL_INDICES;


var _floating_island;


func _generate_block(out_buffer: VoxelBuffer, origin_in_voxels: Vector3i, _lod: int):
	var gz := origin_in_voxels.z
	var gx := origin_in_voxels.x
	
	if not _floating_island.block_within_bounds(gx, gz):
		return;
		
	out_buffer.set_channel_depth(_Top_Side_Channel, VoxelBuffer.DEPTH_32_BIT)
	out_buffer.fill(0, _Top_Side_Channel)
	out_buffer.fill(0, _Front_Side_Channel)
	
	_floating_island.fill_fluids(out_buffer, origin_in_voxels)
