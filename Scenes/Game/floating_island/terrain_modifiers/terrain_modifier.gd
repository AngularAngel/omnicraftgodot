extends Node

const Circular_Heightmap = preload("../heightmaps/circular_heightmap.gd")
const Coord_Noise_Heightmap = preload("../heightmaps/coord_noise_heightmap.gd")
const Noise_Heightmap = preload("../heightmaps/noise_heightmap.gd")
const Interpolated_Heightmap = preload("../heightmaps/interpolated_heightmap.gd")

const GENERATOR_CLASSES = {"Circular_Heightmap": Circular_Heightmap,
						   "Coord_Noise_Heightmap": Coord_Noise_Heightmap,
						   "Noise_Heightmap": Noise_Heightmap,
						   "Interpolated_Heightmap": Interpolated_Heightmap,}

var _first_pass_functions = []
var _first_pass_generators = []

func first_pass(floating_island):
	for first_pass_function in _first_pass_functions:
		var generator = first_pass_function.call(floating_island, self);
		if generator != null:
			_first_pass_generators.append(generator)
			floating_island._generators.append(generator)

func add_first_pass_function(function: Callable):
	_first_pass_functions.append(function);


func add_first_pass_generator(Generator_Class, generator_params: Dictionary):
	add_first_pass_function(func circular_height(floating_island, _terrain_modifier):
		var generator = Generator_Class.new();
		generator.apply_params(floating_island, self, generator_params)
		return generator;
	)
	
func get_generator(floating_island, generator_params):
	if is_instance_of(generator_params, TYPE_CALLABLE):
		return generator_params.call(floating_island, self)
	
	return _first_pass_generators[generator_params]

func second_pass(_floating_island):
	pass;
