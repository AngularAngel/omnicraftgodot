extends "res://Scenes/Game/floating_island/terrain_modifiers/terrain_modifier.gd"

const circular_heightmap_params = {"radius": 128,
								   "length": [5, 15],
								   "max_edge_height": [50, 75],
								   "min_edge_height": [0.0, 0.0],
								   "left_heightmap_factor": [1.0, 3.0, 5.0, 3.0, 5.0],
								   "right_heightmap_factor": [1.0, 3.0, 5.0, 3.0, 5.0],
								   "center_height": [0, 0.10],
								   "rim_interpolation_bias": [1.0, 2.0, 5.0, 4.0, 5.0,],
								   "center_interpolation_bias": [1.0, 3.0, 5.0, 3.0, 5.0],}

const circular_coord_noise_params = {"underlying": 0,
							"amplitude": [10, 20],}

const heightmap_noise_params = {"amplitude": [15, 35],}

const heightmap_noise_coord_noise_params = {"underlying": 2,
											"amplitude": [10, 20],}

const interpolation_params = {"underlying": [1, 3],
							  "factor": [0.05, 0.25],}

const interpolation_coord_noise_params = {"underlying": 4,
							"amplitude": [10, 20],}

const dirt_noise_params = {"apply_to_terrain": false,
						   "allow_negatives": true,
						   "base": 3.5,
						   "amplitude": [3, 5],
						   "frequency": [0.015, 0.045],}

func _init():
	add_first_pass_generator(GENERATOR_CLASSES["Circular_Heightmap"], circular_heightmap_params)
	
	add_first_pass_generator(GENERATOR_CLASSES["Coord_Noise_Heightmap"], circular_coord_noise_params)
	
	add_first_pass_generator(GENERATOR_CLASSES["Noise_Heightmap"], heightmap_noise_params)
	
	add_first_pass_generator(GENERATOR_CLASSES["Coord_Noise_Heightmap"], heightmap_noise_coord_noise_params)
	
	add_first_pass_generator(GENERATOR_CLASSES["Interpolated_Heightmap"], interpolation_params)
	
	add_first_pass_generator(GENERATOR_CLASSES["Coord_Noise_Heightmap"], interpolation_coord_noise_params)
	
	add_first_pass_generator(GENERATOR_CLASSES["Noise_Heightmap"], dirt_noise_params)
	
	add_first_pass_function(func coord_noise(_floating_island, terrain_modifier):
		var circular_heightmap = terrain_modifier._first_pass_generators[0];
			
		var center_interpolation_bias = circular_heightmap._center_interpolation_bias
		circular_heightmap._center_interpolation_bias = func lambda(first, second, dist):
			if dist > 0.2:
				dist = pow(dist, center_interpolation_bias)
			else:
				dist = pow(dist, 1.0 / center_interpolation_bias)
			return int(lerp(first, second, dist));
	)
	
	add_first_pass_function(func add_voxel_filler(floating_island, _terrain_modifier):
		floating_island.add_voxel_filler("Glass Filler")
		var stone_filler = floating_island.add_voxel_filler("Stone Filler")
		stone_filler.stone = floating_island.get_voxels().get_voxel_family_dict("Stone Types")[0]._voxel_ids["Basic"];
		var dirt_filler = floating_island.add_voxel_filler("Variable Dirt Filler")
		dirt_filler.dirt = floating_island.get_voxels().get_voxel_family_dict("Dirt Types")[0]._voxel_ids["Basic"];
		dirt_filler._height_source = _first_pass_generators[6];
		var grass_filler = floating_island.add_voxel_filler("Grass Filler")
		grass_filler.grass_side = floating_island.get_voxels().get_voxel_family_dict("Grass Types")[0]._voxel_ids["Top Side"]
		grass_filler.downwards_grass_side = floating_island.get_voxels().get_voxel_family_dict("Grass Types")[0]._voxel_ids["Downwards Side"]
		var water_filler = floating_island.add_fluid_filler("Water Filler")
		water_filler._water_height = _first_pass_generators[0]._height_map[0] + 5;
		grass_filler._minimum_grass_height = water_filler._water_height + 1;
	)
	
	add_first_pass_function(func coord_noise(floating_island, _terrain_modifier):
		var percent_chance = floating_island.get_voxels()._feature_conditions["Percent Chance"]
		var dirt_noise = _first_pass_generators[6];
		var noise_chance = func rock_noise_chance(feature, _generator, position, random):
			return random.randf() < feature.get_meta("Chance", 0.1) + feature.get_meta("Noise_Multiplier", 0.1) * (feature.get_meta("Noise_Base", 0) + dirt_noise._get_height_at(position.x, position.z))
		
		var modify_feature = func modify_feature(feature, noise_base, noise_mult = 0.5):
			feature._conditions.erase(percent_chance)
			feature._conditions.append(noise_chance)
			feature.set_meta("Noise_Base", noise_base)
			feature.set_meta("Noise_Multiplier", feature.get_meta("Chance", 0.1) * noise_mult)
		
		var stone_type = floating_island.get_voxels().get_voxel_family_dict("Stone Types")[0];
		
		var large_rock = floating_island.add_feature(stone_type._features["Large Rock"])
		modify_feature.call(large_rock, -3, -0.5)
		
		var small_rock = floating_island.add_feature(stone_type._features["Small Rock"])
		modify_feature.call(small_rock, -4, -0.5)
		
		var smaller_rock = floating_island.add_feature(stone_type._features["Smaller Rock"])
		modify_feature.call(smaller_rock, -4, -0.5)
		
		var dirt_type = floating_island.get_voxels().get_voxel_family_dict("Dirt Types")[0];
		
		var large_dirt_patch = floating_island.add_feature(dirt_type._features["Large Dirt Patch"])
		modify_feature.call(large_dirt_patch, -1, -0.5)
		
		var small_dirt_patch = floating_island.add_feature(dirt_type._features["Small Dirt Patch"])
		modify_feature.call(small_dirt_patch, 0, -0.5)
		
		var grass_type = floating_island.get_voxels().get_voxel_family_dict("Grass Types")[0];
		
		var large_grass = floating_island.add_feature(grass_type._features["Large Grass"])
		modify_feature.call(large_grass, -1, 0.5)
		
		var grass = floating_island.add_feature(grass_type._features["Grass"])
		modify_feature.call(grass, -1, 0.5)
		
		var plant_type = floating_island.get_voxels().get_voxel_family_dict("Plant Types")[0];
		
		var large_bush = floating_island.add_feature(plant_type._features["Large Bush"])
		modify_feature.call(large_bush, -1, 0.5)
		
		var bush = floating_island.add_feature(plant_type._features["Bush"])
		modify_feature.call(bush, -1, 0.5)
		
		var berry_bush = floating_island.add_feature(plant_type._features["Berry Bush"])
		modify_feature.call(berry_bush, -1, 0.5)
	)
	
