extends Node

const Peak = preload("res://Scenes/Game/floating_island/terrain_modifiers/primary/peak.gd")
const Chasm = preload("res://Scenes/Game/floating_island/terrain_modifiers/primary/chasm.gd")
const Smooth = preload("res://Scenes/Game/floating_island/terrain_modifiers/secondary/smooth.gd")
const Rugged = preload("res://Scenes/Game/floating_island/terrain_modifiers/secondary/rugged.gd")

var primary = [];
var secondary = [];

func initialize():
	primary.append(Peak);
	primary.append(Chasm)
	
	secondary.append(Smooth)
	secondary.append(Rugged)

func get_primary(_floating_island, random):
	return primary[random.randi_range(0, primary.size() - 1)].new()


func get_secondary(_floating_island, random):
	return secondary[random.randi_range(0, secondary.size() - 1)].new()
