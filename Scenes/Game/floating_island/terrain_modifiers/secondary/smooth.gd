extends "res://Scenes/Game/floating_island/terrain_modifiers/terrain_modifier.gd"

const params = {"amplitude_multiplier": [0.45, 0.85],
				"frequency_multiplier": [0.7, 0.9],}

func second_pass(floating_island):
	for generator in floating_island._generators:
		if generator.get("_noise"):
			generator._noise.frequency *= floating_island._random.randf_range(params["frequency_multiplier"][0], params["frequency_multiplier"][1])
		if generator.get("_noise_x"):
			generator._noise_x.frequency *= floating_island._random.randf_range(params["frequency_multiplier"][0], params["frequency_multiplier"][1])
		if generator.get("_noise_z"):
			generator._noise_z.frequency *= floating_island._random.randf_range(params["frequency_multiplier"][0], params["frequency_multiplier"][1])
		if generator.get("_amplitude"):
			generator._amplitude *= floating_island._random.randf_range(params["amplitude_multiplier"][0], params["amplitude_multiplier"][1])
