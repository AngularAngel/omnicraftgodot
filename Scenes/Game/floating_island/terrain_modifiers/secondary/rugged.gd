extends "res://Scenes/Game/floating_island/terrain_modifiers/terrain_modifier.gd"

const params = {"amplitude_multiplier": [1.25, 3.75],
				"frequency_multiplier": [1.1, 1.3],}

func second_pass(floating_island):
	for generator in floating_island._generators:
		if generator.get("_noise"):
			generator._noise.frequency *= floating_island._random.randf_range(params["frequency_multiplier"][0], params["frequency_multiplier"][1])
		if generator.get("_noise_x"):
			generator._noise_x.frequency *= floating_island._random.randf_range(params["frequency_multiplier"][0], params["frequency_multiplier"][1])
		if generator.get("_noise_z"):
			generator._noise_z.frequency *= floating_island._random.randf_range(params["frequency_multiplier"][0], params["frequency_multiplier"][1])
		if generator.get("_amplitude"):
			generator._amplitude *= floating_island._random.randf_range(params["amplitude_multiplier"][0], params["amplitude_multiplier"][1])
	floating_island._fluid_fillers[0]._water_height += 5;
	floating_island._voxel_fillers[3]._minimum_grass_height += 5;
