extends Node3D

const Heightmap_Generator = preload("./heightmaps/heightmap_generator.gd")
const Structure = preload("../voxels/features/structure.gd")

var _random;

var _modifiers := [];
var _generators := [];

var _features := [];
var _heightmap_generator: Heightmap_Generator;
var _voxel_fillers := [];
var _fluid_fillers := [];


var _radius;
var heights = []
var surfaces = []
var structures = []


var _max_horizontal_feature_dimension = 0;


func initialize(_Voxels, random):
	_random = random
	get_terrain().generator._floating_island = self
	get_terrain().set_meta("isTerrain", true)
	get_fluid_terrain().generator._floating_island = self
	get_fluid_terrain().set_meta("isFluidTerrain", true)
	
	apply_modifiers()
	
	for _voxel_filler in _voxel_fillers:
		_voxel_filler.initialize(self)
	
	for _fluid_filler in _fluid_fillers:
		_fluid_filler.initialize(self)
	
	_generate_heights_and_surfaces();
	_generate_features()
	
	#add_instancer();
	
	get_terrain().mesher.side_library.bake()


func _generate_heights_and_surfaces():
	for x in range(-_radius, _radius + 1):
		heights.append([])
		surfaces.append([])
		structures.append([])
		for z in range(-_radius, _radius + 1):
			heights[x + _radius].append(_heightmap_generator._get_height_at(x, z))
			structures[x + _radius].append([])
	
	for x in range(-_radius, _radius + 1):
		for z in range(-_radius, _radius + 1):
			var height = _get_height_at(x, z)
			var surface = Structure.new()
			surface.offset = Vector3i(x, height - 10, z)
			surface.voxels.create(1, 20, 1)
			fill_voxels(surface.voxels, surface.offset)
			surfaces[x + _radius].append(surface)


func fill_voxels(out_buffer: VoxelBuffer, voxel_position: Vector3i):
	for _voxel_filler in _voxel_fillers:
		var bufferSize := out_buffer.get_size();
		var gx := voxel_position.x
		var gz := voxel_position.z
		
		for z in bufferSize.z:
			gx = voxel_position.x

			for x in bufferSize.x:
				if voxel_within_bounds(gx, gz):
					_voxel_filler.fill_voxels(out_buffer, bufferSize, voxel_position, gx, gz, x, z)
				gx += 1

			gz += 1


func fill_fluids(out_buffer: VoxelBuffer, voxel_position: Vector3i):
	for _fluid_filler in _fluid_fillers:
		var bufferSize := out_buffer.get_size();
		var gz := voxel_position.z
		var gx := voxel_position.x
		
		for z in bufferSize.z:
			gx = voxel_position.x

			for x in bufferSize.x:
				if voxel_within_bounds(gx, gz):
					_fluid_filler.fill_voxels(out_buffer, bufferSize, voxel_position, gx, gz, x, z)
				gx += 1

			gz += 1


func fill_structures(out_buffer: VoxelBuffer, voxel_position: Vector3i):
	var bufferSize := out_buffer.get_size();
	var voxel_tool := out_buffer.get_voxel_tool()
	
	var gz := voxel_position.z - 2
	for z in bufferSize.z + 4:
		var gx := voxel_position.x - 2

		for x in bufferSize.x + 4:
			if voxel_within_bounds(gx, gz, -1):
				var local_structures = _get_structures_at(gx, gz);
				for structure in local_structures:
					var pos = structure.offset - voxel_position;
					voxel_tool.paste(pos, structure.voxels, 0xFF)
			gx += 1
		gz += 1


func _generate_features():
	var rng := RandomNumberGenerator.new()
	rng.seed = _get_chunk_seed_2d(Vector3(0, 0, 0))
	
	for x in range(-_radius, _radius + 1):
		for z in range(-_radius, _radius + 1):
			if voxel_within_bounds(x, z, -1):
				var height := _heightmap_generator._get_height_at(x, z)
				var local_structures = _get_structures_at(x, z);
				for feature in _features:
					var voxel_position = feature.get_position(self, Vector3i(x, height - 1, z), rng)
					if feature.check(self, voxel_position, rng):
						local_structures.append(feature.get_structure(voxel_position))


func add_instancer():
	var instancer = VoxelInstancer.new()

	var library = VoxelInstanceLibrary.new()
	# Usually most of this is done in editor, but some features can only be setup by code atm.
	# Also if we want to procedurally-generate some of this, we may need code anyways.

	var instance_generator = VoxelInstanceGenerator.new()
	instance_generator.min_slope_degrees = 0
	#instance_generator.set_layer_min_height(layer_index, body.radius * 0.95)
	instance_generator.vertical_alignment = 0.0
	instance_generator.emit_mode = VoxelInstanceGenerator.EMIT_FROM_FACES
	instance_generator.noise = FastNoiseLite.new()
	instance_generator.noise.frequency = 1.0 / 16.0
	instance_generator.noise.fractal_octaves = 2
	instance_generator.noise_on_scale = 1
	#instance_generator.noise.noise_type = FastNoiseLite.TYPE_PERLIN
	var item = VoxelInstanceLibraryMultiMeshItem.new()
	
	item.set_mesh(get_voxels()._meshes._dict["Large Cross"], 0)
	
	item.set_material_override(get_voxels()._shaders._vegetation_material)
	
	#instance_generator.density = 0.32
	instance_generator.density = 0.20
	instance_generator.min_scale = 0.8
	instance_generator.max_scale = 1.6
	instance_generator.random_vertical_flip = false
	instance_generator.max_slope_degrees = 30

	item.name = "grass"

	item.generator = instance_generator
	item.persistent = false
	item.lod_index = 0
	library.add_item(0, item)

	instancer.library = library
	get_terrain().add_child(instancer)


func apply_modifiers():
	for modifier in _modifiers:
		modifier.first_pass(self)
	
	for i in range(1, _modifiers.size() + 1):
		_modifiers[-i].second_pass(self)


func add_feature(feature):
	if is_instance_of(feature, TYPE_STRING):
		feature = get_voxels()._feature_dict[feature].copy()
	var dimensions = feature.get_dimensions();
	_max_horizontal_feature_dimension = max(_max_horizontal_feature_dimension, dimensions.x)
	_max_horizontal_feature_dimension = max(_max_horizontal_feature_dimension, dimensions.z)
	_features.append(feature)
	return feature;


func add_voxel_filler(voxel_filler):
	if is_instance_of(voxel_filler, TYPE_STRING):
		voxel_filler = get_voxels()._voxel_fillers.get_voxel_filler(voxel_filler);
	_voxel_fillers.append(voxel_filler)
	return voxel_filler;


func add_fluid_filler(fluid_filler):
	if is_instance_of(fluid_filler, TYPE_STRING):
		fluid_filler = get_voxels()._voxel_fillers.dict[fluid_filler].new();
	_fluid_fillers.append(fluid_filler)
	return fluid_filler;


func get_terrain():
	return $VoxelTerrain;


func get_fluid_terrain():
	return $FluidTerrain;


func get_voxels():
	return get_parent();


func _get_height_at(x: int, z: int):
	return heights[x + _radius][z + _radius];


func _get_surface_at(x: int, z: int):
	return surfaces[x + _radius][z + _radius];


func _get_structures_at(x: int, z: int):
	if x + _radius >= structures.size() || z + _radius >= structures[x + _radius].size():
		return [];
	return structures[x + _radius][z + _radius];


func voxel_within_bounds(gx, gz, offset = 0):
	return _heightmap_generator.voxel_within_bounds(gx, gz, offset)


func block_within_bounds(gx, gz) -> bool:
	return _heightmap_generator.block_within_bounds(gx, gz)


func _get_chunk_seed_2d(cpos: Vector3) -> int:
	return int(cpos.x) ^ (31 * int(cpos.z))
