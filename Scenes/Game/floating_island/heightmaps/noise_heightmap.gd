class_name NoiseHeightmap

extends "./heightmap_generator.gd"

var _noise := FastNoiseLite.new();
var _base := 0;
var _amplitude := 0;
var _allow_negatives := false;

func apply_params(floating_island:Node, terrain_modifier, params: Dictionary):
	_noise.seed = floating_island._random.randi_range(-2^63, 2^63 - 1);
	_base = get_parameter(floating_island, "base", params, 0)
	_amplitude = get_parameter(floating_island, "amplitude", params, 1)
	_noise.frequency = get_parameter(floating_island, "frequency", params, 0.01)
	_allow_negatives = get_parameter(floating_island, "allow_negatives", params, false)
	
	return super.apply_params(floating_island, terrain_modifier, params);


func _get_height_at(x: int, z: int):
	if _allow_negatives:
		return int(_base + _noise.get_noise_2d(x, z) * _amplitude);
	else:
		return int(_base + (0.5 + 0.5 * _noise.get_noise_2d(x, z)) * _amplitude);
