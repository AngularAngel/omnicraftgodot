class_name CircularHeightmap

extends "./heightmap_generator.gd"

var _radius := 0;
var _height_map = [];
var _center_height := 0;
var _rim_interpolation_bias = 1.0;
var _center_interpolation_bias = 1.0;


func apply_params(floating_island: Node, terrain_modifier, params: Dictionary):
	floating_island._radius = params["radius"];
	_radius = params["radius"];
	_radius = get_parameter(floating_island, "radius", params, 0)
	
	var map_resolution = get_parameter(floating_island, "length", params, 5) * 2;
	
	var max_edge_height = get_parameter(floating_island, "max_edge_height", params, 5)
	var min_edge_height = int(max_edge_height * params["min_edge_height"][0] + 
							  params["min_edge_height"][1] * max_edge_height * randf())
	
	var left_heightmap_factor = get_factor(floating_island, params["left_heightmap_factor"])
	var right_heightmap_factor = get_factor(floating_island, params["right_heightmap_factor"])
		
	generate_heights(map_resolution, min_edge_height, max_edge_height, left_heightmap_factor, right_heightmap_factor)
	
	_center_height = floating_island._random.randi_range(max_edge_height * params["center_height"][0], max_edge_height * params["center_height"][0]);
	
	_rim_interpolation_bias = get_factor(floating_island, params["rim_interpolation_bias"])
	_center_interpolation_bias = get_factor(floating_island, params["center_interpolation_bias"])
	
	return super.apply_params(floating_island, terrain_modifier, params);
	
func get_factor(floating_island: Node, params):
	if is_instance_of(params, TYPE_ARRAY):
		if floating_island._random.randf() < params[0]:
			return 1.0 / floating_island._random.randf_range(params[1], params[2])
		else:
			return floating_island._random.randf_range(params[3], params[4])
	else:
		return params;

func generate_heights(length: int, min_edge_height: int, max_edge_height: int,
					  left_heightmap_factor := 1.0, right_heightmap_factor := 1.0):
	_height_map = [];
	
	for i in range(length):
		_height_map.append(0);
	
	var midpoint = _height_map.size() / 2;
	_height_map[0] = min_edge_height;
	_height_map[midpoint] = max_edge_height;
	
	for i in range(1, midpoint):
		_height_map[i] = interpolate(left_heightmap_factor, min_edge_height, max_edge_height, float(i) / midpoint)
		_height_map[-i] = interpolate(right_heightmap_factor, min_edge_height, max_edge_height, float(i) / midpoint)

func voxel_within_bounds(gx, gz, offset = 0) -> bool:
	var radius = _radius + offset;
	if gx > radius or gz > radius:
		return false;
	return gx * gx + gz * gz <= radius * radius

func _get_height_at(x: int, z: int) -> int:
	var rim_height = get_rim_height(x, z)
	
	var dist = sqrt(x * x + z * z) / _radius;
	
	return interpolate(_center_interpolation_bias, _center_height, rim_height, dist)


func get_rim_height(x: int, z: int):
	var first_height = circle_height_index((atan2(float(z), float(x)) + PI) / (PI * 2.0) * (_height_map.size()));
	
	var fract = first_height;
	first_height = int(first_height);
	fract -= first_height;
	
	var second_height = circle_height_index(first_height + 1);
	
	return interpolate(_rim_interpolation_bias, _height_map[first_height], _height_map[second_height], fract)
	
func circle_height_index(height_index):
	if height_index >= _height_map.size():
		height_index -= _height_map.size();
	return height_index;
	
