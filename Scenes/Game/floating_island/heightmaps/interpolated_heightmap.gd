extends "res://Scenes/Game/floating_island/heightmaps/heightmap_generator.gd"
class_name InterpolatedHeightmap

const Heightmap_Generator = preload("./heightmap_generator.gd")

var _first: Heightmap_Generator;
var _second: Heightmap_Generator;

var _factor_source: Callable;
var interpolator = 1;

func apply_params(floating_island:Node, terrain_modifier, params: Dictionary):
	var factor = params["factor"][0] + floating_island._random.randf() * params["factor"][1];
	_factor_source = func _get_factor(_x, _z):
		return factor;
		
		
	_first = terrain_modifier.get_generator(floating_island, params["underlying"][0]);
	
	_second = terrain_modifier.get_generator(floating_island, params["underlying"][1]);
	
	return super.apply_params(floating_island, terrain_modifier, params);


func _get_height_at(x: int, z: int) -> int:
	return interpolate(interpolator, _first._get_height_at(x, z), _second._get_height_at(x, z), _factor_source.call(x, z));


func voxel_within_bounds(gx, gz, offset = 0) -> bool:
	return _first.voxel_within_bounds(gx, gz, offset);
