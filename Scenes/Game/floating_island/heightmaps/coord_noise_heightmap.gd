class_name CoordNoiseHeightmap

extends "res://Scenes/Game/floating_island/heightmaps/heightmap_generator.gd"

const Heightmap_Generator = preload("./heightmap_generator.gd")

var _underlying: Heightmap_Generator;

var _noise_x := FastNoiseLite.new();
var _noise_z := FastNoiseLite.new();

var _amplitude = 0;

func apply_params(floating_island:Node, terrain_modifier, params: Dictionary):
	_underlying = terrain_modifier.get_generator(floating_island, params["underlying"]);
	
	_noise_x.seed = floating_island._random.randi_range(-2^63, 2^63 - 1);
	_noise_z.seed = floating_island._random.randi_range(-2^63, 2^63 - 1);
	
	_amplitude = get_parameter(floating_island, "amplitude", params, 1)
	var frequency = get_parameter(floating_island, "frequency", params, 0.01)
	
	_noise_x.frequency = frequency
	_noise_z.frequency = frequency
	
	return super.apply_params(floating_island, terrain_modifier, params);

func _get_height_at(x: int, z: int) -> int:
	var noise_x = x + _noise_x.get_noise_2d(x, z) * _amplitude;
	var noise_z = z + _noise_z.get_noise_2d(x, z) * _amplitude;
	
	return _underlying._get_height_at(noise_x, noise_z)

func voxel_within_bounds(gx, gz, offset = 0) -> bool:
	return _underlying.voxel_within_bounds(gx, gz, offset);
