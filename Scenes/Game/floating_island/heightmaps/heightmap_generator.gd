class_name HeightmapGenerator

extends Node

const _chunk_size := 16;

func apply_params(floating_island:Node, _terrain_modifier, params: Dictionary):
	if get_parameter(floating_island, "apply_to_terrain", params, true):
		floating_island._heightmap_generator = self;
	return self;


func get_parameter(floating_island, parameter: String, params: Dictionary, default = 0):
	if not params.has(parameter):
		return default;
	var value = params[parameter];
	if is_instance_of(value, TYPE_INT) || is_instance_of(value, TYPE_FLOAT)  || is_instance_of(value, TYPE_BOOL):
		return value;
	if is_instance_of(value, TYPE_ARRAY):
		return floating_island._random.randi_range(value[0], value[1])


func voxel_within_bounds(_gx, _gz, _offset = 0) -> bool:
	return true;


func block_within_bounds(gx, gz) -> bool:
	if gx < -_chunk_size:
		gx += _chunk_size;
	if gz < -_chunk_size:
		gz += _chunk_size;
	
	return voxel_within_bounds(gx, gz);


func _get_height_at(_x: int, _z: int) -> int:
	return 0;


func interpolate(factor, start: int, end: int, dist: float) -> int:
	if is_instance_of(factor, TYPE_CALLABLE):
		return factor.call(start, end, dist)
	return int(lerp(start, end, pow(dist, factor)));
