extends Node


var _interaction_menu;

var _potential_interactions := [];
var _active_interaction : InteractionMethod;

func _ready():
	var parent = get_parent();
	
	await parent.ready
	
	_interaction_menu = parent._interaction_menu;
	
	_potential_interactions.append(parent._interaction_db.get_entry_by_name("eat"))
	_potential_interactions.append(parent._interaction_db.get_entry_by_name("put_down"))
	_potential_interactions.append(parent._interaction_db.get_entry_by_name("crafting"))


func show_options():
	if _active_interaction == null:
		var interactions = _get_interactions();
		if interactions.size() > 0:
			for interaction in interactions:
				_interaction_menu.add_option(interaction, activate_interaction.bind(interaction));
			get_parent().show_interaction_menu();
	else:
		
		if _active_interaction.has_options():
			_active_interaction.show_options(_interaction_menu);
			get_parent().show_interaction_menu();
		else:
			_active_interaction = null;
			show_options();


func _get_interactions():
	var interactions = [];
	for interaction in _potential_interactions:
		if interaction.can_be_used_by(get_parent()._player):
			interactions.append(interaction);
	return interactions;


func _get_applicable_inventory_slots():
	var inventory_slots = [];
	for inventory_slot in get_parent()._player._inventory.get_inventory_slots():
		if _active_interaction.can_be_used_on(inventory_slot._item):
			inventory_slots.append(inventory_slot);
	return inventory_slots;


func activate_interaction(interaction: InteractionMethod):
	interaction.initialize(self)
	_active_interaction = interaction
	_interaction_menu.clear();
	show_options();
