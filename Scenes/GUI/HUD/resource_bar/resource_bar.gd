extends ProgressBar


@export var _resource_name: String


var _resource: ResourceContainer;


func _ready():
	var player = get_parent().get_parent().get_parent();
	
	await player.ready
	
	set_resource(player._stats.get_stat(_resource_name))


func set_resource(value: ResourceContainer):
	_resource = value;
	_resource._current_resource.changed.connect(_on_current_stat_changed)
	_resource._maximum_resource.changed.connect(_on_maximum_stat_changed);
	set_value(_resource._current_resource.get_current_score());
	set_max(_resource._maximum_resource.get_current_score());


func _on_current_stat_changed():
	set_value(_resource._current_resource.get_current_score());


func _on_maximum_stat_changed():
	set_max(_resource._maximum_resource.get_current_score());
