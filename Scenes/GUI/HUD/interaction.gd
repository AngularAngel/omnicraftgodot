extends Node


@onready var _interaction_db : InteractionDB = get_node("/root/Main/Game/Voxels/Interactions")
@onready var _player = get_parent().get_parent();
@onready var _camera : Camera3D = _player.get_node("Camera")
@onready var _interaction_menu = $InteractionMenu
@onready var _crosshair = get_parent().get_node("CrosshairCenterer/Crosshair")
@onready var _overlay = get_parent().get_node("CrosshairCenterer/Overlay")
@onready var _crosshair_sprite = _crosshair.texture;
@onready var _voxel_interaction = $VoxelInteraction
@onready var _entity_interaction = $EntityInteraction
@onready var _self_interaction = $SelfInteraction



func _process(delta):
	if Input.mouse_mode == Input.MOUSE_MODE_CAPTURED:
		if Input.is_action_just_pressed("target"):
			if not _entity_interaction.do_interaction():
				_voxel_interaction.do_interaction();
		elif Input.is_action_just_pressed("untarget"):
			if not _entity_interaction.drop_interaction():
				_voxel_interaction.drop_interaction();
		elif Input.is_action_just_pressed("selfInteraction"):
			_self_interaction.show_options();
				
	elif (Input.is_action_just_pressed("selfInteraction") or Input.is_action_just_pressed("prevMenu")) \
			and _interaction_menu.is_visible_in_tree():
		_interaction_menu.clear();
	elif Input.is_action_just_pressed("untarget") and _interaction_menu.is_visible_in_tree():
		_interaction_menu.clear()
		if _self_interaction._active_interaction != null:
			_self_interaction._active_interaction = null;
			_self_interaction.show_options();
	
	
	_crosshair.texture = _crosshair_sprite;
	_overlay.hide();
	if not _entity_interaction.update_crosshair():
		_voxel_interaction.update_crosshair();


func show_interaction_menu():
	get_parent().get_parent().release_mouse()
	_interaction_menu.show();
