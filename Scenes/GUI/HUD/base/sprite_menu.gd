extends ColorRect

var option_scene = preload("./SpriteTextureButton.tscn");

@onready var _container = $CenterContainer/GridContainer


func clear():
	for child in _container.get_children():
		child.queue_free()
	hide();
	get_parent().get_parent().get_parent().capture_mouse()


func add_option(optionTarget, binding):
	var option = option_scene.instantiate()
	_container.add_child(option)
	option.set_target(optionTarget);
	_container.queue_sort()
	if binding != null:
		option.selected.connect(binding);
	return option;
	
