extends Control

signal selected


@onready var _selected_bg = $SelectedBackground
@onready var _sprite = $Sprite as TextureRect

var _target;

func _gui_input(event):
	if event is InputEventMouseButton:
		if not event.pressed and event.button_index == MOUSE_BUTTON_LEFT:
			emit_signal("selected")


func _notification(what: int):
	match what:
		NOTIFICATION_MOUSE_ENTER:
			_selected_bg.visible = true
		
		NOTIFICATION_MOUSE_EXIT:
			_selected_bg.visible = false

		NOTIFICATION_VISIBILITY_CHANGED:
			if not is_visible_in_tree():
				_selected_bg.visible = false


func set_target(target):
	_target = target;
	if target != null:
		_sprite.texture = target._sprite;
	else:
		_sprite.texture = null;


func get_sprite():
	return _sprite
