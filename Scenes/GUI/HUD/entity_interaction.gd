extends Node


var _interaction_menu;

var _potential_interactions := [];
var _active_interactions := [];

func _ready():
	var parent = get_parent();
	
	await parent.ready
	
	_interaction_menu = parent._interaction_menu;
	
	_potential_interactions.append(parent._interaction_db.get_entry_by_name("pick_up"))


func do_interaction():
	var entity = get_parent()._player._raycast.get_collider();
	
	return _interact_with_entity(entity)


func drop_interaction():
	var entity = get_parent()._player._raycast.get_collider();
	if entity != null:
		for interaction in _active_interactions:
			if interaction.can_be_used_on(entity):
				_active_interactions.erase(interaction)
				return true;
	return false;
		


func _interact_with_entity(entity):
	if entity == null:
		return false;
		
	for interaction in _active_interactions:
		if interaction.can_be_used_on(entity):
			if interaction.is_allowed(entity):
				interaction.interact(entity)
			return true;
	
	var interactions = _get_interactions(entity);
	if interactions.size() > 0:
		for interaction in interactions:
			_interaction_menu.add_option(interaction, activate_interaction.bind(interaction));
		get_parent().show_interaction_menu();
		return true;
	return false;

func _get_interactions(entity):
	var interactions = [];
	for interaction in _potential_interactions:
		if interaction.can_be_used_on(entity):
			interactions.append(interaction);
	return interactions;


func activate_interaction(interaction: InteractionMethod):
	interaction.initialize(self)
	_active_interactions.append(interaction)
	if _active_interactions.size() > 3:
		_active_interactions.remove_at(0)
	_interaction_menu.clear();


func update_crosshair():
	var entity = get_parent()._player._raycast.get_collider();
	
	if entity != null:
		for interaction in _active_interactions:
			if interaction.can_be_used_on(entity):
				get_parent()._crosshair.texture = interaction._sprite;
				if not interaction.is_allowed(entity):
					get_parent()._overlay.show()
				return true;
		for interaction in _potential_interactions:
			if interaction.can_be_used_on(entity):
				return true;
	return false;
	
