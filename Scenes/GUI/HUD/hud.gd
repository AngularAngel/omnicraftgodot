extends Control


# Called when the node enters the scene tree for the first time.
func _ready():
	get_parent().mouse_captured.connect(_on_mouse_captured)
	get_parent().mouse_released.connect(_on_mouse_released)


func _on_mouse_captured():
	$CrosshairCenterer.show();


func _on_mouse_released():
	$CrosshairCenterer.hide();
