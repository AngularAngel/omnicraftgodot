extends Node

const Util = preload("res://Imports/util.gd")

var _interaction_menu;

var _terrain : VoxelTerrain
var _terrain_tool : VoxelTool = null
var _block_outline : MeshInstance3D = null

var _potential_interactions := [];
var _active_interactions := [];

func _ready():
	var mesh := Util.create_wirecube_mesh(Color(0,0,0))
	var mesh_instance := MeshInstance3D.new()
	mesh_instance.mesh = mesh
	mesh_instance.set_scale(Vector3(1,1,1)*1.01)
	_block_outline = mesh_instance
	
	var parent = get_parent();
	
	await parent.ready
	
	_interaction_menu = parent._interaction_menu;
	
	var _interaction_db : InteractionDB = parent._interaction_db;
	
	_terrain = parent._player._terrain
	_terrain.add_child(_block_outline)
	
	_terrain_tool = _terrain.get_voxel_tool()
	_terrain_tool.channel = VoxelBuffer.CHANNEL_TYPE
	
	_potential_interactions.append(_interaction_db.get_entry_by_name("trample"))
	_potential_interactions.append(_interaction_db.get_entry_by_name("harvest"))


func do_interaction():
	var hit = _get_pointed_voxel();
	if hit != null:
		var hit_block_data = _get_hit_voxel_data(hit.position)
		_interact_with_voxel(hit_block_data[0], hit_block_data[1], hit.position)


func drop_interaction():
	var hit := _get_pointed_voxel();
	if hit != null:
		var hit_block_data = _get_hit_voxel_data(hit.position)
		for interaction in _active_interactions:
			if interaction.can_be_used_on(hit_block_data[0]):
				_active_interactions.erase(interaction)
				return;


func _interact_with_voxel(voxel, voxel_position, hit_position):
			for interaction in _active_interactions:
				if interaction.can_be_used_on(voxel):
					if interaction.is_allowed(voxel, voxel_position, hit_position):
						interaction.interact(voxel, voxel_position, hit_position)
					return;
			var interactions = _get_interactions(voxel);
			if interactions.size() > 0:
				for interaction in interactions:
					_interaction_menu.add_option(interaction, activate_interaction.bind(interaction));
				get_parent().show_interaction_menu();


func _get_interactions(voxel: VoxelBlockyModel):
	var interactions = [];
	for interaction in _potential_interactions:
		if interaction.can_be_used_on(voxel):
			interactions.append(interaction);
	return interactions;


func activate_interaction(interaction: InteractionMethod):
	interaction.initialize(self)
	_active_interactions.append(interaction)
	if _active_interactions.size() > 3:
		_active_interactions.remove_at(0)
	_interaction_menu.clear();


func update_crosshair():
	if _terrain == null:
		return
		
	var hit := _get_pointed_voxel()
	
	if hit != null:
		var hit_block_data = _get_hit_voxel_data(hit.position)
		var hit_block = hit_block_data[0];
		var block_position = hit_block_data[1]
		_block_outline.show()
		_block_outline.set_scale(hit_block.get_meta("selectionSize", Vector3i(1, 1, 1)))
		_block_outline.set_position(block_position)
		for interaction in _active_interactions:
			if interaction.can_be_used_on(hit_block):
				get_parent()._crosshair.texture = interaction._sprite;
				if not interaction.is_allowed(hit_block, block_position, hit.position):
					get_parent()._overlay.show()
				return true;
	else:
		_block_outline.hide()
	return false;

func _get_hit_voxel_data(position: Vector3i):
	var hit_block_id := _terrain_tool.get_voxel(position)
	while hit_block_id > 0 and hit_block_id <= 3:
		match (hit_block_id):
			1:
				position.x -= 1;
			2:
				position.y -= 1;
			3:
				position.z -= 1;
		
		hit_block_id = _terrain_tool.get_voxel(position)
	return [get_parent().get_parent().get_parent()._voxel_library.get_model(hit_block_id), position]


func _get_pointed_voxel() -> VoxelRaycastResult:
	var origin = get_parent()._camera.get_global_transform().origin
	var forward = -get_parent()._camera.get_global_transform().basis.z.normalized()
	var hit := _terrain_tool.raycast(origin, forward, 3, 2)
	return hit
