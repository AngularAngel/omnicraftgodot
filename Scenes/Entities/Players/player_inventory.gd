extends Inventory


const ItemOnGround = preload("res://Scenes/Entities/Items/ItemOnGround.tscn")


var _left_hand := InventorySlot.new();
var _right_hand := InventorySlot.new();


func _ready():
	_left_hand.item_changed.connect(func updateLeftHand():
		get_node("../HUD/LeftHand").set_target(_left_hand._item)
	)
	_right_hand.item_changed.connect(func updateLeftHand():
		get_node("../HUD/RightHand").set_target(_right_hand._item)
	)
	
	_left_hand.item_changed.connect(_on_item_changed);
	_left_hand.item_added.connect(_on_item_added);
	_left_hand.item_removed.connect(_on_item_removed);
	
	_right_hand.item_changed.connect(_on_item_changed);
	_right_hand.item_added.connect(_on_item_added);
	_right_hand.item_removed.connect(_on_item_removed);


func can_accept_item(item : Item):
	return _left_hand._item == null or _right_hand._item == null;


func accept_item(item : Item):
	if _left_hand._item == null:
		_left_hand.set_item(item);
	elif _right_hand._item == null:
		_right_hand.set_item(item);
	else:
		var item_on_ground = ItemOnGround.instantiate();
		item_on_ground._terrain = get_parent()._terrain;
		item_on_ground.position = get_parent().get_position()
		get_parent()._terrain.get_parent().add_child(item_on_ground)
		item_on_ground.set_item(item);


func get_inventory_slots():
	var inventory_slots = [];
	if _left_hand._item != null:
		inventory_slots.append(_left_hand);
	if _right_hand._item != null:
		inventory_slots.append(_right_hand);
	return inventory_slots;


func _on_item_changed():
	item_changed.emit();


func _on_item_added():
	item_added.emit();


func _on_item_removed():
	item_removed.emit();
