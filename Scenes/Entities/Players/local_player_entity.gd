extends "res://Scenes/Entities/Base/moving_entity.gd"


signal mouse_captured
signal mouse_released


const WALKING = preload("../movement_modes/walking.gd")
const FLYING = preload("../movement_modes/flying.gd")


@onready var _camera = $Camera as Camera3D
@onready var _stats = $Stats as StatContainer
@onready var _inventory = $Inventory
@onready var _knowledge = $Knowledge
@onready var _raycast = $Camera/Raycast as RayCast3D
var mouseSensibility = 600
var mouse_relative_x = 0
var mouse_relative_y = 0
var JUMP_VELOCITY = 9

var _fluid_tool : VoxelTool;
var _voxel_library: VoxelBlockyLibrary;


func _ready():
	super._ready()
	set_movement_mode(FLYING.new())
	#Captures mouse
	capture_mouse()
	_raycast.add_exception(self)


func get_speed():
	return $Stats.get_stat("MoveSpeed").get_current_score();


func capture_mouse():
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED;
	mouse_captured.emit();


func release_mouse():
	Input.mouse_mode = Input.MOUSE_MODE_VISIBLE;
	mouse_released.emit();


func _physics_process(delta):
	var direction := Vector3();
	var vertical := 0.0
	if Input.mouse_mode == Input.MOUSE_MODE_CAPTURED:
		if Input.is_action_just_pressed("toggleFlight"):
			if _movement_mode.is_affected_by_gravity():
				set_movement_mode(FLYING.new())
			else:
				set_movement_mode(WALKING.new())
		
		# Handle Jump.
		if _movement_mode.is_affected_by_gravity() and Input.is_action_pressed("jump") and _grounded:
			velocity.y = JUMP_VELOCITY
			_grounded = false
		
		# Get the input direction and handle the movement/deceleration.
		var input_dir = Input.get_vector("moveLeft", "moveRight", "moveForward", "moveBackward")
		if not _movement_mode.is_affected_by_gravity():
			if Input.is_action_pressed("moveUp"):
				vertical += 1.0;
			if Input.is_action_pressed("moveDown"):
				vertical += -1.0;
		direction = (transform.basis * Vector3(input_dir.x, vertical, input_dir.y)).normalized()
	move(direction)
	super._physics_process(delta)
	if direction.length_squared() > 0 or vertical != 0 or (_movement_mode.is_affected_by_gravity() and !_grounded):
		var fluid_id = _fluid_tool.get_voxel(get_camera_pos().floor());
		if fluid_id == 0:
			$FluidImmersionPass/FluidImmersionRect.hide();
		else:
			$FluidImmersionPass/FluidImmersionRect.show();
			$FluidImmersionPass/FluidImmersionRect.material = _voxel_library.get_model(fluid_id).get_meta("FluidImmersion")
			$FluidImmersionPass/FluidImmersionRect.material.set_shader_parameter("_camera_pos", get_camera_pos())
	
	var mp := get_tree().get_multiplayer()
	if mp.has_multiplayer_peer():
		# Broadcast our position to other peers.
		# Note, for other peers, this is a different script (remote_character.gd).
		# Each peer is authoritative of its own position for now.
		# TODO Make sure this RPC is not sent when we are not connected
		rpc(&"receive_position", position)


func _input(event):
	if Input.mouse_mode == Input.MOUSE_MODE_CAPTURED and event is InputEventMouseMotion:
		rotation.y -= event.relative.x / mouseSensibility
		_camera.rotation.x -= event.relative.y / mouseSensibility
		_camera.rotation.x = clamp(_camera.rotation.x, deg_to_rad(-90), deg_to_rad(90) )
		mouse_relative_x = clamp(event.relative.x, -50, 50)
		mouse_relative_y = clamp(event.relative.y, -50, 10)


func get_camera_pos():
	return _camera.get_global_transform().origin;


func get_stats():
	return _stats;


@rpc("authority", "call_remote", "unreliable")
func receive_position(_pos: Vector3):
	# We currently don't expect this to be called. The actual targetted script is different.
	# I had to define it otherwise Godot throws a lot of errors everytime I call the RPC...
	push_error("Didn't expect to receive RPC position")
