extends Node

var craftable_items := [];

func _ready():
	var item_db : ItemDB = get_parent()._terrain.get_node("../../../ItemTypes")
	
	craftable_items.append(item_db.get_entry_by_name("basket"))
