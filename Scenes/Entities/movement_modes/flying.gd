extends "res://Scenes/Entities/movement_modes/movement_mode.gd"


func _ready():
	_speed = 15;


func is_affected_by_gravity():
	return false;
	

func move(entity, direction):
	if direction:
		entity.velocity.x = direction.x * entity.get_speed() * get_speed();
		entity.velocity.y = direction.y * entity.get_speed() * get_speed();
		entity.velocity.z = direction.z * entity.get_speed() * get_speed();
	else:
		entity.velocity.x = move_toward(entity.velocity.x, 0, entity.get_speed() * get_speed())
		entity.velocity.y = move_toward(entity.velocity.y, 0, entity.get_speed() * get_speed())
		entity.velocity.z = move_toward(entity.velocity.z, 0, entity.get_speed() * get_speed())
