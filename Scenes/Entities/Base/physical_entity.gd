extends CharacterBody3D

class_name PhysicalEntity

var _grounded = false

# Get the gravity from the project settings to be synced with RigidBody nodes.
var _gravity = ProjectSettings.get_setting("physics/3d/default_gravity")

var _box_mover = VoxelBoxMover.new()

var _collision_box  = AABB(Vector3(-0.4, -0.9, -0.4), Vector3(0.8, 1.8, 0.8))

var _terrain;

func _ready():
	_box_mover.set_step_climbing_enabled(false)
	_box_mover.set_max_step_height(0)
	_box_mover.set_collision_mask(1)
	
func add_gravity(delta):
	velocity.y -= _gravity * delta
	
func _physics_process(delta):
	add_gravity(delta)
	
	var motion : Vector3 = velocity * delta
	var prev_motion = motion
	
	motion = _box_mover.get_motion(get_position(), motion, _collision_box, _terrain)
	global_translate(motion)
	
	# If new motion doesnt move vertically and we were falling before, we just landed
	if abs(motion.y) < 0.001 and prev_motion.y < -0.001:
		_grounded = true

	if _box_mover.has_stepped_up():
		# When we step up, the motion vector will have vertical movement,
		# however it is not caused by falling or jumping, but by snapping the body on
		# top of the step. So after we applied motion, we consider it grounded,
		# and we reset motion.y so we don't induce a "jump" velocity later.
		motion.y = 0
		_grounded = true
	
	# Otherwise, if new motion is moving vertically, we may not be grounded anymore
	elif abs(motion.y) > 0.001:
		_grounded = false
	
	velocity = motion / delta
