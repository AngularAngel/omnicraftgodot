extends "res://Scenes/Entities/Base/physical_entity.gd"


var _movement_mode : MovementMode;


func _ready():
	super._ready()
	_box_mover.set_step_climbing_enabled(true)
	_box_mover.set_max_step_height(1)


func get_speed():
	return 1;


func set_movement_mode(movement_mode):
	if _movement_mode != null:
		remove_child(_movement_mode)
		_movement_mode.queue_free()
	_movement_mode = movement_mode;
	add_child(_movement_mode)
	


func add_gravity(delta):
	if _movement_mode.is_affected_by_gravity():
		super.add_gravity(delta)
		

func move(direction):
	_movement_mode.move(self, direction);
