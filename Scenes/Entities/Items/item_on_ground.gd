extends PhysicalEntity


var _item: Item;
@onready var _sprite = $ItemSprite as Sprite3D


func set_item(item: Item):
	_item = item;
	_sprite.texture = item._sprite
	var sprite_scale = item.get_meta("spriteScale", 1.0);
	_sprite.set_scale(Vector3(sprite_scale, sprite_scale, sprite_scale))

# Called when the node enters the scene tree for the first time.
func _ready():
	super._ready()
	set_meta("isItem", true);
	_collision_box  = AABB(Vector3(-0.4, -0.05, -0.4), Vector3(0.8, 0.1, 0.8))
